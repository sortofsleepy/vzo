
precision mediump float;
precision mediump int;
varying vec4 vColor;

uniform float time;
uniform vec2 resolution;



//include Perlin noise
#pragma glslify: pnoise = require(../modules/pnoise)

varying vec2 vUv;
void main(void) {


  vec2 position = -1.0 + 2.0 * gl_FragCoord.xy / resolution.xy;
  float red = abs(sin(position.x * position.y + time / 2.0)) / 2.0;
  float green = abs(sin(position.x * position.y + time / 5.0)) / 6.0;
  float blue = (cos(position.y * position.x + time)) / 5.0;
  gl_FragColor = vec4(red / 4.0, green / 6.0, blue, 1.0);
}