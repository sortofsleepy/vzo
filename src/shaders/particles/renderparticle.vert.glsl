
#define GLSLIFY 1

precision mediump float;

//are we playing the audio?
uniform int isPlaying;

//timer
uniform float time;

//audio texture
uniform sampler2D audioTexture;

uniform float sizeDelta;

//color of the particles
attribute vec3 cColor;

//size of the particles
attribute float size;


varying vec2 vUv;
varying vec3 vColor;

#pragma glslify: random = require(../modules/cnoise)

void main() {
    vUv = uv;
    vColor = cColor;

    vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );

    vec4 pos = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
    vec4 aTexture = texture2D(audioTexture,position.xy);

    float allSize = size * ( 100.0 / length( mvPosition.xyz ) );

    //if music is playing, give the elements a random size
    int playing = isPlaying;
    if(playing == 1){
        float finalSize = ((size) + length(aTexture.xyz));
        float dx = finalSize - size;
        dx *= 0.05;
        gl_PointSize += sin(time) * random(aTexture.xyz) * 500.0;
        gl_PointSize += allSize;
    }else{
        //otherwise they remain constant
        gl_PointSize = allSize;
    }



    float ran = random( aTexture.xyz);

    gl_Position = pos;
}
