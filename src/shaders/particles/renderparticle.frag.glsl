
//cary over texture coordinates
varying vec2 vUv;
varying vec3 vColor;
uniform int isPlaying;


//visual output for the particle
uniform sampler2D displayTexture;

void main(void) {

    vec3 vertexColor = vColor;

    //grab the texture info
    vec4 particleDisplay = texture2D(displayTexture,gl_PointCoord);

    int playing = isPlaying;
    if(playing == 2){
      vertexColor.x += 200.0;
        vertexColor.y += 200.0;
        vertexColor.z += 200.0;
    }

    //compute the final look
    vec4 finalLook = particleDisplay * vec4(vertexColor,1.0);

    gl_FragColor = finalLook;
}