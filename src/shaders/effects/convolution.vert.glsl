#define KERNEL_SIZE_FLOAT 25.0
#define KERNEL_SIZE_INT 25

uniform vec2 uImageIncrement;
uniform sampler2D tDiffuse;


varying vec2 vUv;
void main(){
    "vUv = uv - ( ( KERNEL_SIZE_FLOAT - 1.0 ) / 2.0 ) * uImageIncrement;",
    "gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );",
}