
attribute vec4 colors;
uniform float oscillator;
varying vec4 vColor;
uniform float time;
uniform sampler2D audioTexture;
attribute vec3 targetVertex;

uniform vec3 randValue;

#pragma glslify: random = require(../modules/cnoise)

void main(){
    vColor = colors;
     float radius = 8.0;
    vec3 vPos = vec3(0.0,0.0,0.0);

    vec4 audioValues = texture2D(audioTexture,uv);

    vec4 pos = projectionMatrix * modelViewMatrix * vec4(position.xyz,1.0);


    gl_Position = pos;
}
