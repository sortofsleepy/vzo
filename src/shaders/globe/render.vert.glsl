uniform sampler2D t_pos;
varying vec2 vUv;


void main(){

 vUv = uv;
  vec4 pos = texture2D( t_pos , position.xy );
  vec3 dif = cameraPosition - pos.xyz;


vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );


gl_PointSize = 20.0 * ( 300.0 / length( mvPosition.xyz ) );
  gl_Position = projectionMatrix * modelViewMatrix * vec4( pos.xyz , 1. );


}
