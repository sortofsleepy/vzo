
uniform float test;
uniform sampler2D overlay;
varying vec2 vUv;

void main(){

    float p = test;
    vec4 oColor = texture2D(overlay,vUv);
    gl_FragColor = oColor;

}
