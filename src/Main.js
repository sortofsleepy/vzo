import PGlobe from "./Globe.js"
import SphereWorld from "./world/SphereWorld.js"

var renderer = new THREE.WebGLRenderer({
    preserveDrawingBuffer:true
});


var scene = new THREE.Scene();
//var camera = new THREE.PerspectiveCamera(75.0,window.innerWidth / window.innerHeight, 1, 1000);
//camera.position.z = 100;

var camera = new THREE.PerspectiveCamera( 27, window.innerWidth / window.innerHeight, 1, 3500 );
camera.position.z = 1050;
camera.position.y = -215;

renderer.setSize(window.innerWidth,window.innerHeight);
renderer.setPixelRatio(window.devicePixelRatio);

document.body.appendChild(renderer.domElement);

/**============ SPHERE WORLD ===============*/
var sphere = new SphereWorld({
    radius: 900,
    widthSegments: 68,
    heightSegments: 66
});

var material = new THREE.MeshPhongMaterial( {
    color: 0xaaaaaa, specular: 0xffffff, shininess: 250,
    side: THREE.DoubleSide, vertexColors: THREE.VertexColors
} );

//var mesh = new THREE.Mesh( sphere.getGeometry(), material );
var mesh = new THREE.Mesh( sphere.getGeometry(), material );
scene.add( sphere.getGeometry() );

/**============ MAIN GLOBE ===============*/
var globe = new PGlobe(1000,renderer);
globe.addTo(scene);

//setup trackball control
var trackball = new THREE.TrackballControls(camera,renderer.domElement);


window.addEventListener("resize",function(){
    ASPECT_RATIO = window.innerWidth / window.innerHeight;
    camera.aspect = ASPECT_RATIO;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
});



animate();
function animate(){
    window.requestAnimationFrame(animate);
    globe.updateSim();
    trackball.update();


    renderer.render(scene,camera);
}
