
var glslify = require("glslify");

/**
 * Describes the nebula like background
 */
class Nebula {
    constructor(scene){
        this.timer = new THREE.Clock();

        //make the geometry
        this.geo = new THREE.PlaneBufferGeometry(window.innerWidth,window.innerHeight,1,1);

        //grab and compile our shaders
        var shader = glslify({
            vertex:"../shaders/background.vert.glsl",
            fragment:"../shaders/background.frag.glsl",
            sourceOnly:true
        });

        shader.uniforms.time = 0.4;
        //build out material
        this.material = new THREE.RawShaderMaterial({
            uniforms :{
                time:{
                    type:'f',
                    value:1.0
                },
                resolution:{
                    type:'v2',
                    value:new THREE.Vector2(window.innerWidth,window.innerHeight)
                }
            },
            vertexShader:shader.vertex,
            fragmentShader:shader.fragment,
            side:THREE.DoubleSide,
            transparent:true
        });

        this.mesh = new THREE.Mesh(this.geo,this.material);
      //  scene.add(this.mesh);

        this.update();

    }

    /**
     * Nebula will manage it's own animation
     */
    update(){
        var parent = this;
        animate();
        function animate(){
            window.requestAnimationFrame(animate);
            var time = performance.now();
            parent.mesh.material.uniforms.time.value = time / 1000;
        }
    }
}

export default Nebula;
