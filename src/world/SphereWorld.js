class SphereWorld extends THREE.Geometry {
    constructor(options,gui) {

        THREE.Geometry.call(this);

        this.type = 'SphereGeometry';

        this.defaults = {
            radius: 50,
            widthSegments: Math.random()*99,
            heightSegments: 6,
            phiStart: 0,
            phiLength: Math.PI * 2,
            thetaStart: 0,
            thetaLength: Math.PI
        };

        options = options != undefined ? options : this.defaults;

        options = this._extend(options);



        this._buildGeometry(gui);

    }

    _buildGeometry(gui) {
        var options = this.options;
        var radius = options.radius;


        var widthSegments = options.widthSegments;
        var heightSegments = options.heightSegments

        var phiStart = options.phiStart;
        var phiLength = options.phiLength;

        var thetaStart = options.thetaStart;
        var thetaLength = options.thetaLength;


        var x, y, vertices = [], uvs = [];

        for (y = 0; y <= heightSegments; y++) {

            var verticesRow = [];
            var uvsRow = [];

            for (x = 0; x <= widthSegments; x++) {

                var u = x / widthSegments;
                var v = y / heightSegments;

                var vertex = new THREE.Vector3();
                vertex.x = -radius * Math.cos(phiStart + u * phiLength) * Math.sin(thetaStart + v * thetaLength);
                vertex.y = radius * Math.cos(thetaStart + v * thetaLength);
                vertex.z = radius * Math.sin(phiStart + u * phiLength) * Math.sin(thetaStart + v * thetaLength);

                this.vertices.push(vertex);

                verticesRow.push(this.vertices.length - 1);
                uvsRow.push(new THREE.Vector2(u, 1 - v));

            }

            vertices.push(verticesRow);
            uvs.push(uvsRow);

        }

        for (y = 0; y < heightSegments; y++) {

            for (x = 0; x < widthSegments; x++) {

                var v1 = vertices[y][x + 1];
                var v2 = vertices[y][x];
                var v3 = vertices[y + 1][x];
                var v4 = vertices[y + 1][x + 1];

                var n1 = this.vertices[v1].clone().normalize();
                var n2 = this.vertices[v2].clone().normalize();
                var n3 = this.vertices[v3].clone().normalize();
                var n4 = this.vertices[v4].clone().normalize();

                var uv1 = uvs[y][x + 1].clone();
                var uv2 = uvs[y][x].clone();
                var uv3 = uvs[y + 1][x].clone();
                var uv4 = uvs[y + 1][x + 1].clone();

                if (Math.abs(this.vertices[v1].y) === radius) {

                    uv1.x = ( uv1.x + uv2.x ) / 2;
                    this.faces.push(new THREE.Face3(v1, v3, v4, [n1, n3, n4]));
                    this.faceVertexUvs[0].push([uv1, uv3, uv4]);

                } else if (Math.abs(this.vertices[v3].y) === radius) {

                    uv3.x = ( uv3.x + uv4.x ) / 2;
                    this.faces.push(new THREE.Face3(v1, v2, v3, [n1, n2, n3]));
                    this.faceVertexUvs[0].push([uv1, uv2, uv3]);

                } else {

                    this.faces.push(new THREE.Face3(v1, v2, v4, [n1, n2, n4]));
                    this.faceVertexUvs[0].push([uv1, uv2, uv4]);

                    this.faces.push(new THREE.Face3(v2, v3, v4, [n2.clone(), n3, n4.clone()]));
                    this.faceVertexUvs[0].push([uv2.clone(), uv3, uv4.clone()]);

                }

            }
        }

        this.computeFaceNormals();

        this.boundingSphere = new THREE.Sphere(new THREE.Vector3(), radius);

        ////////// SETUP BUFFER GEOMETRY /////////

        var triangles = 160000;
        var n = 800, n2 = n/2;	// triangles spread in the cube
        var d = 12, d2 = d/2;	// individual triangle size

        var points = this.vertices;
        var flat = [];
        for(var i = 0;i<points.length;++i){
            flat.push(points[i].x);
            flat.push(points[i].y);
            flat.push(points[i].z);
        }


        var particles = flat.length;

        var geometry = new THREE.BufferGeometry();

        var positions = new Float32Array( particles );
        var colors = new Float32Array( particles );

        var color = new THREE.Color();

        var n = 1000, n2 = n / 2; // particles spread in the cube

        for ( var i = 0; i < positions.length; i += 3 ) {

            // positions

            var x = Math.random() * n - n2;
            var y = Math.random() * n - n2;
            var z = Math.random() * n - n2;

            positions[ i ]     = flat[ i ]    ;
            positions[ i + 1 ] = flat[ i + 1 ];
            positions[ i + 2 ] = flat[ i + 2 ];

            // colors

            var vx = ( x / n ) + 0.5;
            var vy = ( y / n ) + 0.5;
            var vz = ( z / n ) + 0.5;

            color.setRGB( vx, vy, vz );

            colors[ i ]     = color.r;
            colors[ i + 1 ] = color.g;
            colors[ i + 2 ] = color.b;

        }

        geometry.addAttribute( 'position', new THREE.BufferAttribute( positions, 3 ) );
        geometry.addAttribute( 'color', new THREE.BufferAttribute( colors, 3 ) );

        //console.log(positions);
        geometry.computeBoundingSphere();
        var material = new THREE.PointCloudMaterial( { size: 155, vertexColors: THREE.VertexColors } );
        // var material = new THREE.ShaderMaterial({
//
        //     vertexShader:document.getElementById("vertex-s").textContent,
        //     fragmentShader:document.getElementById("fragment-s").textContent
        // });
        var particleSystem = new THREE.Mesh( geometry, material );


        particleSystem.rotation.x = 140;


        this.geo = particleSystem;

    }

    rotateY(deg){
        this.geo.rotation.y = deg;
    }

    rotateX(deg){
        this.geo.rotation.x = deg;
    }

    rotateZ(deg){
        this.geo.rotation.z = deg;
    }

    getGeometry(){
        return this.geo;
    }

    _extend(options){

        for(var i in this.defaults){
            if(!options.hasOwnProperty(i)){
                options[i] = this.defaults[i];
            }
        }

        this.options = options;

        return this.options;
    }
}

export default SphereWorld
