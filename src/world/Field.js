
import Composer from "../Composer"
import AudioTexture from "../audio/AudioTexture.js"
var glslify = require("glslify");

/**
 * Particle field for the simulation
 */
class Field extends Composer{
    constructor(size,scene,renderer){
        super(size,renderer);
        this.size = size;
        this.color = new THREE.Color();
        this.renderer = renderer;
        this.scene = scene;
        this.timer = new THREE.Clock();
        this.audioTexture = "";

        //build the mesh to display
        this._buildVbo();

        //composer for doing animations
        var composer = new Composer(size/2,renderer);

        //uniforms for rendering
        this.uniforms = {
            t_pos:{
                type:'t',
                value:null
            },

            //texture for how to display the particle
            //Texture is reported as 124x124
            displayTexture:{
                type:'t',
                value:THREE.ImageUtils.loadTexture( "./img/flare.png" )
            },

            audioTexture:{
                type:'t',
                value:null
            },

            fieldSize:{
                type:'i',
                value:0
            },

            isPlaying:{
                type:'i',
                value:0
            },

            time:{
                type:'f',
                value:0.0
            }
        };

        this.attributes = {
            cColor:{
                type:'c',
                value:new THREE.Color(0xffffff)
            },

            size:{
                type:'f',
                value:1.0
            }
    };

        //uniforms to use in the calculations
        this.sUniforms = {
            delta:{
                type:'f',
                value:0
            }
        };

        this.shader = glslify({
            vertex:"../shaders/particles/renderparticle.vert.glsl",
            fragment:"../shaders/particles/renderparticle.frag.glsl",
            sourceOnly:true
        });

        this.mat = new THREE.ShaderMaterial({
            attributes:this.attributes,
            uniforms:this.uniforms,
            vertexShader:this.shader.vertex,
            fragmentShader:this.shader.fragment,
            blending:       THREE.AdditiveBlending,
            depthTest:      false,
            transparent:    true
        });


        var cloud = new THREE.PointCloud(this.geo,this.mat);
        cloud.frustrumCulled = false;
        this.field = cloud;
        this.uniforms.fieldSize.value = this.size;
        scene.add(cloud);
    }

    /**
     * Field manages it's own animation
     */
    updateField(){

        var parent = this;
        animate();

        function animate(){
            window.requestAnimationFrame(animate);

            parent.uniforms.time.value = Date.now() * 0.005;


            if(window.isPlayingAudio === true){
                parent.uniforms.isPlaying.value = 1;
            }else{
                parent.uniforms.isPlaying.value = 0;
            }
            parent.uniforms.audioTexture = parent.audioTexture;
        }
    }

    setAudioTexture(audioTexture){
        this.audioTexture = audioTexture;
    }

    _buildVbo(){
        var size = this.size;
        var color = this.color;
        var particles = new THREE.BufferGeometry();
        var radius = 8.0;


        //build "Vbo" to display
        var geo = new THREE.BufferGeometry();
        var positions = new Float32Array(size * 3);
        var colors = new Float32Array(size * 3);
        var sizes = new Float32Array(size);

        for(var i = 0;i<positions.length;++i){

            sizes[i] = Math.random() * 3.0;

            positions[ i * 3 + 0 ] = ( Math.random() * 2 - 1 ) * radius;
            positions[ i * 3 + 1 ] = ( Math.random() * 2 - 1 ) * radius;
            positions[ i * 3 + 2 ] = ( Math.random() * 2 - 1 ) * radius;

            this.color.setHSL( i / positions.length, 1.0, 0.5 );

            colors[ i * 3 ] = color.r;
            colors[ i * 3 + 1 ] = color.g;
            colors[ i * 3 + 2 ] = color.b;
        }


        var posA = new THREE.BufferAttribute( positions , 3 );
        var cColor = new THREE.BufferAttribute(colors,3);
        var pSize = new THREE.BufferAttribute(sizes,1);

        geo.addAttribute( 'position', posA );
        geo.addAttribute('cColor',cColor);
        geo.addAttribute('size',pSize);

        this.geo = geo;

    }


}

export default Field;
