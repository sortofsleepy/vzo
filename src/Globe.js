
import Composer from "./Composer"
import AudioTexture from "./audio/AudioTexture"

var glslify = require("glslify");


class Globe extends Composer{
    constructor(size,renderer,options){
        super(size,renderer,options);

        //remember, this is a path relative to the public folder
        var audio = new AudioTexture("./audio/mother.mp3");
        audio.update();


        var parent = this;
        window.addEventListener("keydown",function(){
            audio.play();
            var playVal = parent.uniforms.isPlaying.value;

            if(playVal == 1){
                parent.uniforms.isPlaying.value = 0;
            }else{
                parent.uniforms.isPlaying.value = 1;
            }
        });

        var i = 0;


        var texture = new TG.Texture( 256, 256 )
            .add( new TG.XOR().tint( 1, 0.5, 0.7 ) )
            .add( new TG.SinX().frequency( 0.004 ).tint( 0.5, 0, 0 ) )
            .mul( new TG.SinY().frequency( 0.004 ).tint( 0.5, 0, 0 ) )
            .add( new TG.SinX().frequency( 0.0065 ).tint( 0.1, 0.5, 0.2 ) )
            .add( new TG.SinY().frequency( 0.0065 ).tint( 0.5, 0.5, 0.5 ) )
            .add( new TG.Noise().tint( 0.1, 0.1, 0.2 ) )
            .toCanvas();

        this.threeTexture = new THREE.Texture(texture);
        this.threeTexture.needsUpdate = true;

        /////// SHADERS //////
        //load render shaders
        var renderShader = glslify({
            vertex:"./shaders/globe/render.vert.glsl",
            fragment:"./shaders/globe/render.frag.glsl",
            sourceOnly:true
        });

        //load sim shader
        var simulationSource = glslify({
            vertex:"./shaders/globe/gravity.glsl",
            fragment:"./shaders/globe/gravity.glsl",
            sourceOnly:true
        });

        this.uniforms.audioTexture = {
            type:'t',
            value:audio.getTexture()
        };

        this.uniforms.beatTexture = {
            type:'t',
            value:audio.getBeatsTexture()
        };

        //add the pass
        this.addPass("gravity",simulationSource.fragment,{
            dT:{ type:"f" , value: 0 },
            isPlaying:{
                type:'i',
                value:0
            },
            overlay:{
                type:'t',
                value:null
            },
            centerPoss: { type:"v3" , value: new THREE.Vector3() }
        });




        this.audio = audio;
        this.mat = new THREE.ShaderMaterial({
            uniforms:{
                test:{
                    type:'f',
                    value:1.0
                }
            },
            vertexShader: renderShader.vertex,
            fragmentShader: renderShader.fragment,
        });

        this.generateGeometry(size)


        /// OBJECT TO RENDER////
        this.particles = new THREE.PointCloud( this.geo , this.mat );
        this.particles.frustumCulled = false;




    }

    updateSim(){
        this.update();

    }

    generateGeometry(particleCount){
        var size = particleCount


        var particleCount = 1000;
        var r = 1.0

        //amount at which the vertices move
        this.oscillatorSpeed = 0.5;

        //holds limits of how far vertices can move
        this.targetVertices = [];

        //setup arrays
        var positions = new Float32Array(particleCount * 3);
        var colors = new Float32Array(particleCount * 4);

        //size of all the vertices
        this.psize = positions;

        this.radius = 0;
        //internal reference to positions array
        this.positions = positions;

        //loop through and create max distance references
        for(var i = 0;i<positions.length;++i){
            var vec = new THREE.Vector3();
            vec.x = Math.random();
            vec.y = Math.random();
            vec.z = Math.random();
            vec.normalize();
            vec.multiplyScalar( r );

            this.targetVertices.push(vec.x);
            this.targetVertices.push(vec.y);
            this.targetVertices.push(vec.z);
        }


        //initial positions start at 0; targettargetVertices reference a max distance
        for(var i = 0;i<positions.length;++i){
            positions[i] = this.targetVertices[i]
            positions[i + 1] = this.targetVertices[i + 1]
            positions[i + 2] = this.targetVertices[i + 2]

            //positions[i] = 200;
            //positions[i + 1] = this.targetVertices[i + 1];
            //positions[i + 2] =0;
        }

        //a value used for ocillation
        this.oscillator = 0;

        //make new geometry
        var geometry = new THREE.BufferGeometry();

        //setup colors
        var color = new THREE.Color();
        for(var i = 0;i<colors.length;++i){
            color.setHSL( i / particleCount, 1.0, 0.5 );

            colors[i] = color.r;
            colors[i + 1] = color.g;
            colors[i + 2] = color.b;
            colors[i + 3] = Math.random();
        }

        //add attributes
        geometry.addAttribute("position",new THREE.BufferAttribute(positions,3));
        geometry.addAttribute("colors",new THREE.BufferAttribute(colors,4));

        //add target vertices as another attribute
        geometry.addAttribute("targetVertex",new THREE.BufferAttribute(new Float32Array(this.targetVertices),3));

        //setup the globe!!!
        this.parameters = [ [ 0.25, new THREE.Color(0xB2FDFF), 1, 2 ], [ 0.5, new THREE.Color(0xA2E8D6), 1, 1 ], [ 0.75, new THREE.Color(0xBCFFB2), 0.75, 1 ], [ 1,new THREE.Color( 0xA2E8AE), 0.5, 1 ], [ 1.25, new THREE.Color(0x000833), 0.8, 1 ]];
        this.length = this.parameters.length;
        this.lines = [];

        for( i = 0; i < this.parameters.length; ++ i ) {

            var p = this.parameters[ i ];

            var line = new THREE.Line( geometry, this.mat );
            line.scale.x = line.scale.y = line.scale.z = p[ 0 ];
            line.originalScale = p[ 0 ];
            line.rotation.y = Math.random() * Math.PI;
            line.updateMatrix();
            this.lines.push(line);
        }

        //internal reference to geometry
        this.geo = geometry

    }


    addTo(scene){
        for(var i = 0;i<this.lines.length;++i){
            scene.add(this.lines[i]);
        }

        //  scene.add(this.particles)
    }


}

export default Globe;
