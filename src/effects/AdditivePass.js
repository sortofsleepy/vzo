import WAGNER from "../../effect_composer/Wagner.js"
var glslify = require("glslify");

class AdditivePass extends WAGNER.Pass {
    constructor(){
        var shader = glslify({
            vertex:"../../shaders/effects/additive.vert.glsl",
            fragment:"../../shaders/effects/additive.frag.glsl",
            sourceOnly:true
        });

        this.shader = WAGNER.processShader(shader.vertex,shader.fragment);
    }
}

export default AdditivePass;