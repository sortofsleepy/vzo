
import WAGNER from "../../effect_composer/Wagner.js"
var glslify = require("glslify");

class BlurPass extends WAGNER.Pass {
    constructor(){
        var shaders = glslify({
            vertex:"../../shaders/blur.vert.glsl",
            fragment:"../../shaders/blur.frag.glsl",
            sourceOnly:true
        });

        this.shader = WAGNER.processshader(shaders.vertex,shaders.fragment);
    }

}

export default BlurPass;