
import WAGNER from "../../effect_composer/Wagner.js"
var glslify = require("glslify");

class CopyPass extends WAGNER.Pass {
    constructor(){
        var shader = glslify({
            vertex:"../../shaders/effects/copy.vert.glsl",
            fragment:"../../shaders/effects/copy.frag.glsl",
            sourceOnly:true
        });

        this.shader = WAGNER.processShader(shader.vertex,shader.fragment);
    }
}

export default CopyPass;