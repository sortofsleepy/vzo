import WAGNER from "../../effect_composer/Wagner.js"
var glslify = require("glslify");

class ConvolutionPass extends WAGNER.Pass{
    constructor(){

    }

    buildKernel(sigma){
        function guass(x,sigma){
            return Math.exp(- (x * x) / (2.0 * sigma * sigma));
        }

        var i,values,sum,halfWidth,kMaxKernelSize=25;
        var kernelSize = 2 * Math.ceil(sigma * 3.0) + 1;

        if(kernelSize > kMaxKernelSize){
            kernelSize = kMaxKernelSize;

        }

        halfWidth = (kernelSize - 1) * 0.5;
        values = [kernelSize];
        sum = 0.0;
        for(var i = 0;i<kernelSize;++i){
            values[i] = gauss(i - halfWidth,sigma);
            sum += values[i];
        }

        //normalize
        for(var i = 0;i<kernelSize;++i){
            values[i] /= sum;
        }

        return values;
    }
}