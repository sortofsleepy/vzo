
/**
 * Masking pass for THREE adapted to ES6
 * @author alteredq / http://alteredqualia.com/
 */
class MaskPass {
    constructor(scene,camera){
        this.scene = scene;
        this.camera = camera;
        this.enabled = true;
        this.needsSwap = false;
        this.inverse = false;
    }

    render(renderer,writeBuffer,readBuffer,delta){
        var ctx = renderer.context;
        ctx.colorMask(false,false,false,false);
        ctx.depthMask(false);

        var writeValue,clearValue;

        if(this.inverse){
            writeValue = 0;
            clearValue = 1;
        }else{
            writeValue = 1;
            clearValue = 0;
        }

        ctx.enable(ctx.STENCIL_TEST);
        ctx.stencilOp(ctx.REPLACE,ctx.REPLACE,ctx.REPLACE);
        ctx.stencilFunc(ctx.ALWAYS,writeValue, 0xffffffff);
        ctx.clearStencil();

        renderer.render(this.scene,this.camera,readBuffer,this.clear);
        renderer.render(this.scene,this.camera,writeBuffer,this.clear);

        //re-enable color and depth
        ctx.colorMask(true,true,true,true);
        ctx.stencilFunc(ctx.EQUAL,1,0xffffffff);//draw if == 1
        ctx.stencilOp(ctx.KEEP,ctx.KEEP,ctx.KEEP);
    }

    //clears the mask
    clearMask(renderer,writeBuffer,readBuffer,delta){
        var ctx = renderer.getContext();
        ctx.disable(ctx.STENCIL_TEST);
    }
}

export default MaskPass;