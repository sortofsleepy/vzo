import WAGNER from "../../effect_composer/Wagner.js"
var glslify = require("glslify");

class FXAAShader extends WAGNER.Pass {
    constructor(){
        var shaders = glslify({
            vertex:"../../shaders/effects/fxaa.vert.glsl",
            fragment:"../../shaders/effects/fxaa.frag.glsl"
        });

        this.shader = WAGNER.processShader(shaders.vertex,shaders.fragment);
    }
}

export default FXAAShader;