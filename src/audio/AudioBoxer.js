/**
 * A class for dealing with WebAudio and WebGL.
 *
 * Based off of the work of some amazing artists.
 *
 * http://cabbi.bo/dotter/lib/AudioController.js
 * http://www.airtightinteractive.com/demos/js/uberviz/audioanalysis
 * @sortofsleepy
 */
class AudioBoxer {
    constructor(file,options){

        var defaults = {
            frequencyBinCount:1024,
            looping:false,
            gain:0.5,
            fadeTime:0.4,
            fftSize:1024,
            smoothingTimeConstant:0.8,
            volumeSensitivity:0.5,
            levelsCount:16
        };

        this.options = options !== undefined ? options : defaults;

        if(options !== undefined){
            //TODO currently needs polyfill
            //Object.assign(this.options,defaults);
        }

        var ctx = window.AudioContext || window.webkitAudioContext;
        this.ctx = ctx = new ctx();

        this.isPlaying = false;

        this.pitchCurrent = 0.0;
        this.pitchLerped = 0.0;
        this.volumeCurrent = 0.0;
        this.volumeLerped = 0.0;
        this.lerpFactor = 0.2;

        this.waveData = [];
        this.levelsData = [];
        this.level = 0;
        this.bpmTime = 0;
        this.ratedBPMTime = 550;
        this.levelHistory = [];

        //BPM related
        this.bpmStart = 1;
        this.BEAT_HOLD_TIME = 40;
        this.BEAT_DECAY_RATE = 0.98;
        this.BEAT_MIN = 0.15;
        this.beatTime = 0;
        this.count = 0;
        this.msecsFirst = 0;
        this.msecsPrevious = 0;
        this.msecsAvg = 633; // time between beats.
        this.levelBins = Math.floor(this.options.frequencyBinCount / this.options.levelsCount); //number of bins in each level
        this.beatCutOff = 0;
        this.gotBeat = false;

        //setup some nodes
        this.analyser = ctx.createAnalyser();
        this.analyser.array = new Uint8Array(this.options.frequencyBinCount);
        this.freqByteData = new Uint8Array( this.analyser.frequencyBinCount );
        this.timeByteData = new Uint8Array(this.options.frequencyBinCount);

        this.filter = ctx.createBiquadFilter();
        this.gain = this.ctx.createGain();

        this.analyser.frequencyBinCount = 128;
        this.analyser.array = new Uint8Array(128);

        this.file = file !== undefined ? file : false;
        var element = new Audio();

        //load audio if it's not undefined
        if(this.file !== false){
            /**
             * Alternate method of streaming audio using <audio> instead of
             * having to directly load the sound first
             */
            this.src = ctx.createMediaElementSource(element);


            element.src = this.file;

            //need to make sure this appends to the body, otherwise the play() function doesn't work.
            element.style.position = "absolute";
            element.style.opacity = 0;
            document.body.appendChild(element);

            this.domElement = element;


            //connect things if we have audio
            this.connectComponents();
        }
    }

    /**
     * Connects all the nodes together
     */
    connectComponents(source){
        this.gain.connect(this.analyser);
        this.src.connect(this.gain);
        this.src.connect(this.ctx.destination);
    }

    /**
     * Fades out the track.
     * @param time
     * @param callback
     */
    fadeOut(time,callback){
        var t = this.ctx.currentTime;
        if(!time){
            time = this.options.fadeTime;
        }
        this.gain.linearRampToValueAtTime(this.gain.value,t);
        this.gain.linearRampToValueAtTime(0.0,t + time);
        setTimeout(callback.bind(this),time * 1000);
    }

    /**
     * Fades in track
     * @param time
     * @param value
     */
    fadeIn(time,value){
        this.gain.linearRampToValueAtTime(1,this.ctx.currentTime + time);
    }

    /**
     * Updates the analyser with new data
     */
    updateData(){
        if(this.isPlaying){
            this.analyser.getByteFrequencyData(this.analyser.array);
            this.analyser.getByteFrequencyData(this.freqByteData);
            this.analyser.getByteTimeDomainData(this.timeByteData);

            //normalize waveform
            this.normalizeWaveform();

            //normalize levels
            this.normalizeLevels();
        }
    }

    normalizeWaveform(){

        for(var i = 0;i < this.analyser.frequencyBinCount;++i){
            this.waveData[i] = ((this.timeByteData[i] - 128) / 128) * this.options.volumeSensitivity
        }

    }

    normalizeLevels(){

        for(var i = 0;i<this.options.levelsCount;++i){
            var sum = 0;
            for(var j = 0; j < this.levelBins; j++) {
                sum += this.freqByteData[(i * this.levelBins) + j];
            }
            this.levelsData[i] = sum / this.levelBins/256 *this.options.volumeSensitivity; //freqData maxs at 256

            //adjust for the fact that lower levels are percieved more quietly
            //make lower levels smaller
            //levelsData[i] *=  1 + (i/levelsCount)/2;
        }

        //set the average level
        var sum = 0;
        for(var j = 0;j < this.options.levelsCount;++j){
            sum += this.levelsData[j]
        }

        this.level = sum / this.options.levelsCount;
        this.levelHistory.push(this.level);
        // this.levelHistory.shift();

        this.computeBeat();
    }

    computeBeat(){
        if(this.level > this.beatCutOff && this.level > this.BEAT_MIN){
            this.beatCutOff = this.level * 1.1;
            this.beatTime = 0;
        }else{
            if(this.beatTime <= this.BEAT_HOLD_TIME){
                this.beatTime++;
            }else{
                this.beatCutOff *= this.BEAT_DECAY_RATE;
                this.beatCutOff = Math.max(this.beatCutOff,this.BEAT_MIN);
            }
        }
        // console.log(this.bpmStart,this.msecsAvg);
        this.bpmTime = (new Date().getTime() - this.bpmStart) / this.msecsAvg;

    }

    /**======================= AUDIO PLAYBACK ====================*/

    play(){
        if(this.isPlaying){
            this.isPlaying = false;
            this.domElement.pause();
        }else{
            this.domElement.play();
            this.isPlaying = true;
        }
    }

    /**
     * Resets the playhead to the beginning
     */
    playFromBeginning(){
        this.domElement.currentTime = 0;
    }

}

export default AudioBoxer;