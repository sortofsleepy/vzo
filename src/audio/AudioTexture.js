
/**
 * A class for dealing with WebAudio,WebGL and texture data.
 * Based off of the work of others.
 *
 * http://www.airtightinteractive.com/demos/js/uberviz/audioanalysis/js/AudioHandler.js
 * http://cabbi.bo/
 *
 * @sortofsleepy
 */
import AudioBoxer from "./AudioBoxer.js"
class AudioTexture extends AudioBoxer {
    /**
     * Constructor
     * @param file path to audio file
     * @param options any additional options we want for the audio processor
     */
    constructor(file,options){
        super(file,options);

        this.width = this.options.frequencyBinCount / 4;
        this.notes = [];
        var data = this.processAudio();
        var beats = this.processBeats();
        /**
         * If we're using THREE, init a data texture
         *
         * TODO include ability to create generalized WebGL texture;
         */
        if(THREE !== undefined){
            this.texture = new THREE.DataTexture(data,data.length / 16, 1, THREE.RGBAFormat,THREE.FloatType);
            this.beatsTexture = new THREE.DataTexture(beats,beats.length / 16, 1, THREE.RGBAFormat,THREE.FloatType);
            this.texture.needsUpdate = true;
        }

    }

    /**
     * Updates our audio data.
     * No need to put this in render loop
     */
    update() {

        var parent = this;

        update();
        function update(){
            window.requestAnimationFrame(update);
            parent.updateData();

            //update texture with audio data
            parent.texture.image.data = parent.processAudio();
            parent.texture.needsUpdate = true;
        }
    }

    /**
     * Returns a texture containing general audio data
     * @returns {AudioTexture.texture|*}
     */
    getTexture(){
        return this.texture;
    }

    /**
     * Returns the texture holding the level history
     * @returns {AudioTexture.beatsTexture|*}
     */
    getBeatsTexture(){
        return this.beatsTexture;
    }

    /**
     * Returns a normalized bpm time
     * @returns {THREE.Vector2.x|*}
     */
    getBpmTime(){
        var vec = new THREE.Vector2(this.bpmTime,0);
        vec.normalize();
        return vec.x;
    }

    //TODO figure out if this is actually a way to process beats
    processBeats(){
        var data = new Float32Array(this.width * 4);
        var beats = this.bpmTime;
        var levels = this.levelHistory;

        for(var i = 0;i<this.width; i += 4){
            data[i] = levels[i] / 256;
            data[i + 1] = levels[i + 1] / 256;
            data[i + 2] = levels[i + 2 ] / 256;
            data[i + 3] = levels[i + 3] / 256;
        }

        return data;
    }


    processAudio(){

        //TODO maybe move this out and re-fill instead of creating new one each time
        var data = new Float32Array(this.width * 4);
        var beats = this.bpmTime;
        var levels = this.levelHistory;

        for(var i = 0;i<this.width;i += 4){
            data[i] = this.analyser.array[(i/4)] / 256;
            data[i + 1] = this.analyser.array[(i/4) + 1] / 256;
            data[i + 2] = this.analyser.array[(i/4) + 2] / 256;
            data[i + 3] = this.analyser.array[(i/4) + 3] / 256;
        }



        return data;
    }
}


export default AudioTexture;

