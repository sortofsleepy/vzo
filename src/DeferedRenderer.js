/**
 * @author alteredq / http://alteredqualia.com/
 * @author MPanknin / http://www.redplant.de/
 */

var WebGLDeferredRenderer = function ( parameters ) {

    var _this = this;

    var pixelWidth = parameters.width !== undefined ? parameters.width : 800;
    var pixelHeight = parameters.height !== undefined ? parameters.height : 600;
    var currentScale = parameters.scale !== undefined ? parameters.scale : 1;

    var scaledWidth = Math.floor( currentScale * pixelWidth );
    var scaledHeight = Math.floor( currentScale * pixelHeight );

    var brightness = parameters.brightness !== undefined ? parameters.brightness : 1;
    var tonemapping = parameters.tonemapping !== undefined ? parameters.tonemapping : THREE.SimpleOperator;
    var antialias = parameters.antialias !== undefined ? parameters.antialias : false;

    this.renderer = parameters.renderer;

    if ( this.renderer === undefined ) {

        this.renderer = new THREE.WebGLRenderer( { antialias: false } );
        this.renderer.setSize( pixelWidth, pixelHeight );
        this.renderer.setClearColor( 0x000000, 0 );

        this.renderer.autoClear = false;

    }

    this.domElement = this.renderer.domElement;

    //

    var gl = this.renderer.context;

    //

    var currentCamera = null;
    var projectionMatrixInverse = new THREE.Matrix4();

    var positionVS = new THREE.Vector3();
    var directionVS = new THREE.Vector3();
    var tempVS = new THREE.Vector3();

    var rightVS = new THREE.Vector3();
    var normalVS = new THREE.Vector3();
    var upVS = new THREE.Vector3();

    //

    var geometryLightSphere = new THREE.SphereGeometry( 1, 16, 8 );
    var geometryLightPlane = new THREE.PlaneBufferGeometry( 2, 2 );

    var black = new THREE.Color( 0x000000 );

    var colorShader = THREE.ShaderDeferred[ "color" ];
    var normalDepthShader = THREE.ShaderDeferred[ "normalDepth" ];

    //

    var emissiveLightShader = THREE.ShaderDeferred[ "emissiveLight" ];
    var pointLightShader = THREE.ShaderDeferred[ "pointLight" ];
    var spotLightShader = THREE.ShaderDeferred[ "spotLight" ];
    var directionalLightShader = THREE.ShaderDeferred[ "directionalLight" ];
    var hemisphereLightShader = THREE.ShaderDeferred[ "hemisphereLight" ];
    var areaLightShader = THREE.ShaderDeferred[ "areaLight" ];

    var compositeShader = THREE.ShaderDeferred[ "composite" ];

    //

    var compColor, compNormal, compDepth, compLight, compFinal;
    var passColor, passNormal, passDepth, passLightFullscreen, passLightProxy, compositePass;

    var effectFXAA;

    //

    var lightSceneFullscreen, lightSceneProxy;

    //

    var resizableMaterials = [];

    //

    var invisibleMaterial = new THREE.ShaderMaterial();
    invisibleMaterial.visible = false;


    var defaultNormalDepthMaterial = new THREE.ShaderMaterial( {

        uniforms:       THREE.UniformsUtils.clone( normalDepthShader.uniforms ),
        vertexShader:   normalDepthShader.vertexShader,
        fragmentShader: normalDepthShader.fragmentShader,
        blending:		THREE.NoBlending

    } );

    //

    var initDeferredMaterials = function ( object ) {

        if ( object.material instanceof THREE.MeshFaceMaterial ) {

            var colorMaterials = [];
            var normalDepthMaterials = [];

            var materials = object.material.materials;

            for ( var i = 0, il = materials.length; i < il; i ++ ) {

                var deferredMaterials = createDeferredMaterials( materials[ i ] );

                if ( deferredMaterials.transparent ) {

                    colorMaterials.push( invisibleMaterial );
                    normalDepthMaterials.push( invisibleMaterial );

                } else {

                    colorMaterials.push( deferredMaterials.colorMaterial );
                    normalDepthMaterials.push( deferredMaterials.normalDepthMaterial );

                }

            }

            object.userData.colorMaterial = new THREE.MeshFaceMaterial( colorMaterials );
            object.userData.normalDepthMaterial = new THREE.MeshFaceMaterial( normalDepthMaterials );

        } else {

            var deferredMaterials = createDeferredMaterials( object.material );

            object.userData.colorMaterial = deferredMaterials.colorMaterial;
            object.userData.normalDepthMaterial = deferredMaterials.normalDepthMaterial;
            object.userData.transparent = deferredMaterials.transparent;

        }

    };

    var createDeferredMaterials = function ( originalMaterial ) {

        var deferredMaterials = {};

        // color material
        // -----------------
        // 	diffuse color
        //	specular color
        //	shininess
        //	diffuse map
        //	vertex colors
        //	alphaTest
        // 	morphs

        var uniforms = THREE.UniformsUtils.clone( colorShader.uniforms );
        var defines = { "USE_MAP": !! originalMaterial.map, "USE_ENVMAP": !! originalMaterial.envMap, "GAMMA_INPUT": true };

        var material = new THREE.ShaderMaterial( {

            fragmentShader: colorShader.fragmentShader,
            vertexShader: 	colorShader.vertexShader,
            uniforms: 		uniforms,
            defines: 		defines,
            shading:		originalMaterial.shading

        } );

        if ( originalMaterial instanceof THREE.MeshBasicMaterial ) {

            var diffuse = black;
            var emissive = originalMaterial.color;

        } else {

            var diffuse = originalMaterial.color;
            var emissive = originalMaterial.emissive !== undefined ? originalMaterial.emissive : black;

        }

        var specular = originalMaterial.specular !== undefined ? originalMaterial.specular : black;
        var shininess = originalMaterial.shininess !== undefined ? originalMaterial.shininess : 1;
        var wrapAround = originalMaterial.wrapAround !== undefined ? ( originalMaterial.wrapAround ? -1 : 1 ) : 1;
        var additiveSpecular = originalMaterial.metal !== undefined ? ( originalMaterial.metal ? 1 : -1 ) : -1;

        uniforms.emissive.value.copyGammaToLinear( emissive );
        uniforms.diffuse.value.copyGammaToLinear( diffuse );
        uniforms.specular.value.copyGammaToLinear( specular );
        uniforms.shininess.value = shininess;
        uniforms.wrapAround.value = wrapAround;
        uniforms.additiveSpecular.value = additiveSpecular;

        uniforms.map.value = originalMaterial.map;

        if ( originalMaterial.envMap ) {

            uniforms.envMap.value = originalMaterial.envMap;
            uniforms.useRefract.value = originalMaterial.envMap.mapping instanceof THREE.CubeRefractionMapping;
            uniforms.refractionRatio.value = originalMaterial.refractionRatio;
            uniforms.combine.value = originalMaterial.combine;
            uniforms.reflectivity.value = originalMaterial.reflectivity;
            uniforms.flipEnvMap.value = ( originalMaterial.envMap instanceof THREE.WebGLRenderTargetCube ) ? 1 : -1;

            uniforms.samplerNormalDepth.value = compNormalDepth.renderTarget2;
            uniforms.viewWidth.value = scaledWidth;
            uniforms.viewHeight.value = scaledHeight;

            resizableMaterials.push( { "material": material } );

        }

        material.vertexColors = originalMaterial.vertexColors;
        material.morphTargets = originalMaterial.morphTargets;
        material.morphNormals = originalMaterial.morphNormals;
        material.skinning = originalMaterial.skinning;

        material.alphaTest = originalMaterial.alphaTest;
        material.wireframe = originalMaterial.wireframe;

        // uv repeat and offset setting priorities
        //	1. color map
        //	2. specular map
        //	3. normal map
        //	4. bump map

        var uvScaleMap;

        if ( originalMaterial.map ) {

            uvScaleMap = originalMaterial.map;

        } else if ( originalMaterial.specularMap ) {

            uvScaleMap = originalMaterial.specularMap;

        } else if ( originalMaterial.normalMap ) {

            uvScaleMap = originalMaterial.normalMap;

        } else if ( originalMaterial.bumpMap ) {

            uvScaleMap = originalMaterial.bumpMap;

        }

        if ( uvScaleMap !== undefined ) {

            var offset = uvScaleMap.offset;
            var repeat = uvScaleMap.repeat;

            uniforms.offsetRepeat.value.set( offset.x, offset.y, repeat.x, repeat.y );

        }

        deferredMaterials.colorMaterial = material;

        // normal + depth material
        // -----------------
        //	vertex normals
        //	morph normals
        //	bump map
        //	bump scale
        //  clip depth

        if ( originalMaterial.morphTargets || originalMaterial.skinning || originalMaterial.bumpMap ) {

            var uniforms = THREE.UniformsUtils.clone( normalDepthShader.uniforms );
            var defines = { "USE_BUMPMAP": !!originalMaterial.bumpMap };

            var normalDepthMaterial = new THREE.ShaderMaterial( {

                uniforms:       uniforms,
                vertexShader:   normalDepthShader.vertexShader,
                fragmentShader: normalDepthShader.fragmentShader,
                shading:		originalMaterial.shading,
                defines:		defines,
                blending:		THREE.NoBlending

            } );

            normalDepthMaterial.morphTargets = originalMaterial.morphTargets;
            normalDepthMaterial.morphNormals = originalMaterial.morphNormals;
            normalDepthMaterial.skinning = originalMaterial.skinning;

            if ( originalMaterial.bumpMap ) {

                uniforms.bumpMap.value = originalMaterial.bumpMap;
                uniforms.bumpScale.value = originalMaterial.bumpScale;

                var offset = originalMaterial.bumpMap.offset;
                var repeat = originalMaterial.bumpMap.repeat;

                uniforms.offsetRepeat.value.set( offset.x, offset.y, repeat.x, repeat.y );

            }

        } else {

            var normalDepthMaterial = defaultNormalDepthMaterial.clone();

        }

        normalDepthMaterial.wireframe = originalMaterial.wireframe;
        normalDepthMaterial.vertexColors = originalMaterial.vertexColors;

        deferredMaterials.normalDepthMaterial = normalDepthMaterial;

        //

        deferredMaterials.transparent = originalMaterial.transparent;

        return deferredMaterials;

    };

    var updatePointLightProxy = function ( lightProxy ) {

        var light = lightProxy.userData.originalLight;
        var uniforms = lightProxy.material.uniforms;

        // skip infinite pointlights
        // right now you can't switch between infinite and finite pointlights
        // it's just too messy as they use different proxies

        var distance = light.distance;

        if ( distance > 0 ) {

            lightProxy.scale.set( 1, 1, 1 ).multiplyScalar( distance );
            uniforms[ "lightRadius" ].value = distance;

            positionVS.setFromMatrixPosition( light.matrixWorld );
            positionVS.applyMatrix4( currentCamera.matrixWorldInverse );

            uniforms[ "lightPositionVS" ].value.copy( positionVS );

            lightProxy.position.setFromMatrixPosition( light.matrixWorld );

        } else {

            uniforms[ "lightRadius" ].value = Infinity;

        }

        // linear space colors

        var intensity = light.intensity * light.intensity;

        uniforms[ "lightIntensity" ].value = intensity;
        uniforms[ "lightColor" ].value.copyGammaToLinear( light.color );

    };

    var createDeferredPointLight = function ( light ) {

        // setup light material

        var materialLight = new THREE.ShaderMaterial( {

            uniforms:       THREE.UniformsUtils.clone( pointLightShader.uniforms ),
            vertexShader:   pointLightShader.vertexShader,
            fragmentShader: pointLightShader.fragmentShader,

            blending:		THREE.AdditiveBlending,
            depthWrite:		false,
            transparent:	true,

            side: THREE.BackSide

        } );

        // infinite pointlights use full-screen quad proxy
        // regular pointlights use sphere proxy

        var  geometry;

        if ( light.distance > 0 ) {

            geometry = geometryLightSphere;

        } else {

            geometry = geometryLightPlane;

            materialLight.depthTest = false;
            materialLight.side = THREE.FrontSide;

        }

        materialLight.uniforms[ "viewWidth" ].value = scaledWidth;
        materialLight.uniforms[ "viewHeight" ].value = scaledHeight;

        materialLight.uniforms[ 'samplerColor' ].value = compColor.renderTarget2;
        materialLight.uniforms[ 'samplerNormalDepth' ].value = compNormalDepth.renderTarget2;

        // create light proxy mesh

        var meshLight = new THREE.Mesh( geometry, materialLight );

        // keep reference for color and intensity updates

        meshLight.userData.originalLight = light;

        // keep reference for size reset

        resizableMaterials.push( { "material": materialLight } );

        // sync proxy uniforms to the original light

        updatePointLightProxy( meshLight );

        return meshLight;

    };

    var updateSpotLightProxy = function ( lightProxy ) {

        var light = lightProxy.userData.originalLight;
        var uniforms = lightProxy.material.uniforms;

        var viewMatrix = currentCamera.matrixWorldInverse;
        var modelMatrix = light.matrixWorld;

        positionVS.setFromMatrixPosition( modelMatrix );
        positionVS.applyMatrix4( viewMatrix );

        directionVS.setFromMatrixPosition( modelMatrix );
        tempVS.setFromMatrixPosition( light.target.matrixWorld );
        directionVS.sub( tempVS );
        directionVS.normalize();
        directionVS.transformDirection( viewMatrix );

        uniforms[ "lightPositionVS" ].value.copy( positionVS );
        uniforms[ "lightDirectionVS" ].value.copy( directionVS );

        uniforms[ "lightAngle" ].value = light.angle;
        uniforms[ "lightDistance" ].value = light.distance;

        // linear space colors

        var intensity = light.intensity * light.intensity;

        uniforms[ "lightIntensity" ].value = intensity;
        uniforms[ "lightColor" ].value.copyGammaToLinear( light.color );

    };

    var createDeferredSpotLight = function ( light ) {

        // setup light material

        var uniforms = THREE.UniformsUtils.clone( spotLightShader.uniforms );

        var materialLight = new THREE.ShaderMaterial( {

            uniforms:       uniforms,
            vertexShader:   spotLightShader.vertexShader,
            fragmentShader: spotLightShader.fragmentShader,

            blending:		THREE.AdditiveBlending,
            depthWrite:		false,
            depthTest:		false,
            transparent:	true

        } );

        uniforms[ "viewWidth" ].value = scaledWidth;
        uniforms[ "viewHeight" ].value = scaledHeight;

        uniforms[ 'samplerColor' ].value = compColor.renderTarget2;
        uniforms[ 'samplerNormalDepth' ].value = compNormalDepth.renderTarget2;

        // create light proxy mesh

        var meshLight = new THREE.Mesh( geometryLightPlane, materialLight );

        // keep reference for color and intensity updates

        meshLight.userData.originalLight = light;

        // keep reference for size reset

        resizableMaterials.push( { "material": materialLight } );

        // sync proxy uniforms to the original light

        updateSpotLightProxy( meshLight );

        return meshLight;

    };

    var updateDirectionalLightProxy = function ( lightProxy ) {

        var light = lightProxy.userData.originalLight;
        var uniforms = lightProxy.material.uniforms;

        directionVS.setFromMatrixPosition( light.matrixWorld );
        tempVS.setFromMatrixPosition( light.target.matrixWorld );
        directionVS.sub( tempVS );
        directionVS.normalize();
        directionVS.transformDirection( currentCamera.matrixWorldInverse );

        uniforms[ "lightDirectionVS" ].value.copy( directionVS );

        // linear space colors

        var intensity = light.intensity * light.intensity;

        uniforms[ "lightIntensity" ].value = intensity;
        uniforms[ "lightColor" ].value.copyGammaToLinear( light.color );

    };

    var createDeferredDirectionalLight = function ( light ) {

        // setup light material

        var uniforms = THREE.UniformsUtils.clone( directionalLightShader.uniforms );

        var materialLight = new THREE.ShaderMaterial( {

            uniforms:       uniforms,
            vertexShader:   directionalLightShader.vertexShader,
            fragmentShader: directionalLightShader.fragmentShader,

            blending:		THREE.AdditiveBlending,
            depthWrite:		false,
            depthTest:		false,
            transparent:	true

        } );

        uniforms[ "viewWidth" ].value = scaledWidth;
        uniforms[ "viewHeight" ].value = scaledHeight;

        uniforms[ 'samplerColor' ].value = compColor.renderTarget2;
        uniforms[ 'samplerNormalDepth' ].value = compNormalDepth.renderTarget2;

        // create light proxy mesh

        var meshLight = new THREE.Mesh( geometryLightPlane, materialLight );

        // keep reference for color and intensity updates

        meshLight.userData.originalLight = light;

        // keep reference for size reset

        resizableMaterials.push( { "material": materialLight } );

        // sync proxy uniforms to the original light

        updateDirectionalLightProxy( meshLight );

        return meshLight;

    };

    var updateHemisphereLightProxy = function ( lightProxy ) {

        var light = lightProxy.userData.originalLight;
        var uniforms = lightProxy.material.uniforms;

        directionVS.setFromMatrixPosition( light.matrixWorld );
        directionVS.normalize();
        directionVS.transformDirection( currentCamera.matrixWorldInverse );

        uniforms[ "lightDirectionVS" ].value.copy( directionVS );

        // linear space colors

        var intensity = light.intensity * light.intensity;

        uniforms[ "lightIntensity" ].value = intensity;
        uniforms[ "lightColorSky" ].value.copyGammaToLinear( light.color );
        uniforms[ "lightColorGround" ].value.copyGammaToLinear( light.groundColor );

    };

    var createDeferredHemisphereLight = function ( light ) {

        // setup light material

        var uniforms = THREE.UniformsUtils.clone( hemisphereLightShader.uniforms );

        var materialLight = new THREE.ShaderMaterial( {

            uniforms:       uniforms,
            vertexShader:   hemisphereLightShader.vertexShader,
            fragmentShader: hemisphereLightShader.fragmentShader,

            blending:		THREE.AdditiveBlending,
            depthWrite:		false,
            depthTest:		false,
            transparent:	true

        } );

        uniforms[ "viewWidth" ].value = scaledWidth;
        uniforms[ "viewHeight" ].value = scaledHeight;

        uniforms[ 'samplerColor' ].value = compColor.renderTarget2;
        uniforms[ 'samplerNormalDepth' ].value = compNormalDepth.renderTarget2;

        // create light proxy mesh

        var meshLight = new THREE.Mesh( geometryLightPlane, materialLight );

        // keep reference for color and intensity updates

        meshLight.userData.originalLight = light;

        // keep reference for size reset

        resizableMaterials.push( { "material": materialLight } );

        // sync proxy uniforms to the original light

        updateHemisphereLightProxy( meshLight );

        return meshLight;

    };

    var updateAreaLightProxy = function ( lightProxy ) {

        var light = lightProxy.userData.originalLight;
        var uniforms = lightProxy.material.uniforms;

        var modelMatrix = light.matrixWorld;
        var viewMatrix = currentCamera.matrixWorldInverse;

        positionVS.setFromMatrixPosition( modelMatrix );
        positionVS.applyMatrix4( viewMatrix );

        uniforms[ "lightPositionVS" ].value.copy( positionVS );

        rightVS.copy( light.right );
        rightVS.transformDirection( modelMatrix );
        rightVS.transformDirection( viewMatrix );

        normalVS.copy( light.normal );
        normalVS.transformDirection( modelMatrix );
        normalVS.transformDirection( viewMatrix );

        upVS.crossVectors( rightVS, normalVS );
        upVS.normalize();

        uniforms[ "lightRightVS" ].value.copy( rightVS );
        uniforms[ "lightNormalVS" ].value.copy( normalVS );
        uniforms[ "lightUpVS" ].value.copy( upVS );

        uniforms[ "lightWidth" ].value = light.width;
        uniforms[ "lightHeight" ].value = light.height;

        uniforms[ "constantAttenuation" ].value = light.constantAttenuation;
        uniforms[ "linearAttenuation" ].value = light.linearAttenuation;
        uniforms[ "quadraticAttenuation" ].value = light.quadraticAttenuation;

        // linear space colors

        var intensity = light.intensity * light.intensity;

        uniforms[ "lightIntensity" ].value = intensity;
        uniforms[ "lightColor" ].value.copyGammaToLinear( light.color );

    };

    var createDeferredAreaLight = function ( light ) {

        // setup light material

        var uniforms = THREE.UniformsUtils.clone( areaLightShader.uniforms );

        var materialLight = new THREE.ShaderMaterial( {

            uniforms:       uniforms,
            vertexShader:   areaLightShader.vertexShader,
            fragmentShader: areaLightShader.fragmentShader,

            blending:		THREE.AdditiveBlending,
            depthWrite:		false,
            depthTest:		false,
            transparent:	true

        } );

        uniforms[ "viewWidth" ].value = scaledWidth;
        uniforms[ "viewHeight" ].value = scaledHeight;

        uniforms[ 'samplerColor' ].value = compColor.renderTarget2;
        uniforms[ 'samplerNormalDepth' ].value = compNormalDepth.renderTarget2;

        // create light proxy mesh

        var meshLight = new THREE.Mesh( geometryLightPlane, materialLight );

        // keep reference for color and intensity updates

        meshLight.userData.originalLight = light;

        // keep reference for size reset

        resizableMaterials.push( { "material": materialLight } );

        // sync proxy uniforms to the original light

        updateAreaLightProxy( meshLight );

        return meshLight;

    };

    var createDeferredEmissiveLight = function () {

        // setup light material

        var materialLight = new THREE.ShaderMaterial( {

            uniforms:       THREE.UniformsUtils.clone( emissiveLightShader.uniforms ),
            vertexShader:   emissiveLightShader.vertexShader,
            fragmentShader: emissiveLightShader.fragmentShader,
            depthTest:		false,
            depthWrite:		false,
            blending:		THREE.NoBlending

        } );

        materialLight.uniforms[ "viewWidth" ].value = scaledWidth;
        materialLight.uniforms[ "viewHeight" ].value = scaledHeight;

        materialLight.uniforms[ 'samplerColor' ].value = compColor.renderTarget2;

        // create light proxy mesh

        var meshLight = new THREE.Mesh( geometryLightPlane, materialLight );

        // keep reference for size reset

        resizableMaterials.push( { "material": materialLight } );

        return meshLight;

    };

    var initDeferredProperties = function ( object ) {

        if ( object.userData.deferredInitialized ) return;

        if ( object.material ) initDeferredMaterials( object );

        if ( object instanceof THREE.PointLight ) {

            var proxy = createDeferredPointLight( object );

            if ( object.distance > 0 ) {

                lightSceneProxy.add( proxy );

            } else {

                lightSceneFullscreen.add( proxy );

            }

        } else if ( object instanceof THREE.SpotLight ) {

            var proxy = createDeferredSpotLight( object );
            lightSceneFullscreen.add( proxy );

        } else if ( object instanceof THREE.DirectionalLight ) {

            var proxy = createDeferredDirectionalLight( object );
            lightSceneFullscreen.add( proxy );

        } else if ( object instanceof THREE.HemisphereLight ) {

            var proxy = createDeferredHemisphereLight( object );
            lightSceneFullscreen.add( proxy );

        } else if ( object instanceof THREE.AreaLight ) {

            var proxy = createDeferredAreaLight( object );
            lightSceneFullscreen.add( proxy );

        }

        object.userData.deferredInitialized = true;

    };

    //

    var setMaterialColor = function ( object ) {

        if ( object.material ) {

            if ( object.userData.transparent ) {

                object.material = invisibleMaterial;

            } else {

                object.material = object.userData.colorMaterial;

            }

        }

    };

    var setMaterialNormalDepth = function ( object ) {

        if ( object.material ) {

            if ( object.userData.transparent ) {

                object.material = invisibleMaterial;

            } else {

                object.material = object.userData.normalDepthMaterial;

            }

        }

    };

    // external API

    this.setAntialias = function ( enabled ) {

        antialias = enabled;

        if ( antialias ) {

            effectFXAA.enabled = true;
            compositePass.renderToScreen = false;

        } else {

            effectFXAA.enabled = false;
            compositePass.renderToScreen = true;
        }

    };

    this.getAntialias = function () {

        return antialias;

    };

    this.addEffect = function ( effect, normalDepthUniform, colorUniform ) {

        if ( effect.material && effect.uniforms ) {

            if ( normalDepthUniform ) effect.uniforms[ normalDepthUniform ].value = compNormalDepth.renderTarget2;
            if ( colorUniform )    	  effect.uniforms[ colorUniform ].value = compColor.renderTarget2;

            if ( normalDepthUniform || colorUniform ) {

                resizableMaterials.push( { "material": effect.material, "normalDepth": normalDepthUniform, "color": colorUniform } );

            }

        }

        compFinal.insertPass( effect, -1 );

    };

    this.setScale = function ( scale ) {

        currentScale = scale;

        scaledWidth = Math.floor( currentScale * pixelWidth );
        scaledHeight = Math.floor( currentScale * pixelHeight );

        compNormalDepth.setSize( scaledWidth, scaledHeight );
        compColor.setSize( scaledWidth, scaledHeight );
        compLight.setSize( scaledWidth, scaledHeight );
        compFinal.setSize( scaledWidth, scaledHeight );

        compColor.renderTarget2.shareDepthFrom = compNormalDepth.renderTarget2;
        compLight.renderTarget2.shareDepthFrom = compNormalDepth.renderTarget2;

        for ( var i = 0, il = resizableMaterials.length; i < il; i ++ ) {

            var materialEntry = resizableMaterials[ i ];

            var material = materialEntry.material;
            var uniforms = material.uniforms;

            var colorLabel = materialEntry.color !== undefined ? materialEntry.color : 'samplerColor';
            var normalDepthLabel = materialEntry.normalDepth !== undefined ? materialEntry.normalDepth : 'samplerNormalDepth';

            if ( uniforms[ colorLabel ] ) uniforms[ colorLabel ].value = compColor.renderTarget2;
            if ( uniforms[ normalDepthLabel ] ) uniforms[ normalDepthLabel ].value = compNormalDepth.renderTarget2;

            if ( uniforms[ 'viewWidth' ] ) uniforms[ "viewWidth" ].value = scaledWidth;
            if ( uniforms[ 'viewHeight' ] ) uniforms[ "viewHeight" ].value = scaledHeight;

        }

        compositePass.uniforms[ 'samplerLight' ].value = compLight.renderTarget2;

        effectFXAA.uniforms[ 'resolution' ].value.set( 1 / pixelWidth, 1 / pixelHeight );

    };

    this.setSize = function ( width, height ) {

        pixelWidth = width;
        pixelHeight = height;

        this.renderer.setSize( pixelWidth, pixelHeight );

        this.setScale( currentScale );

    };

    //

    function updateLightProxy ( proxy ) {

        var uniforms = proxy.material.uniforms;

        if ( uniforms[ "matProjInverse" ] ) uniforms[ "matProjInverse" ].value = projectionMatrixInverse;
        if ( uniforms[ "matView" ] ) uniforms[ "matView" ].value = currentCamera.matrixWorldInverse;

        var originalLight = proxy.userData.originalLight;

        if ( originalLight ) {

            proxy.visible = originalLight.visible;

            if ( originalLight instanceof THREE.PointLight ) {

                updatePointLightProxy( proxy );

            } else if ( originalLight instanceof THREE.SpotLight ) {

                updateSpotLightProxy( proxy );

            } else if ( originalLight instanceof THREE.DirectionalLight ) {

                updateDirectionalLightProxy( proxy );

            } else if ( originalLight instanceof THREE.HemisphereLight ) {

                updateHemisphereLightProxy( proxy );

            } else if ( originalLight instanceof THREE.AreaLight ) {

                updateAreaLightProxy( proxy );

            }

        }

    };

    this.render = function ( scene, camera ) {

        // setup deferred properties

        if ( ! scene.userData.lightSceneProxy ) {

            scene.userData.lightSceneProxy = new THREE.Scene();
            scene.userData.lightSceneFullscreen = new THREE.Scene();

            var meshLight = createDeferredEmissiveLight();
            scene.userData.lightSceneFullscreen.add( meshLight );

        }

        currentCamera = camera;

        lightSceneProxy = scene.userData.lightSceneProxy;
        lightSceneFullscreen = scene.userData.lightSceneFullscreen;

        passColor.camera = currentCamera;
        passNormalDepth.camera = currentCamera;
        passLightProxy.camera = currentCamera;
        passLightFullscreen.camera = new THREE.OrthographicCamera( -1, 1, 1, -1, 0, 1 );

        passColor.scene = scene;
        passNormalDepth.scene = scene;
        passLightFullscreen.scene = lightSceneFullscreen;
        passLightProxy.scene = lightSceneProxy;

        scene.traverse( initDeferredProperties );

        // update scene graph only once per frame
        // (both color and normalDepth passes use exactly the same scene state)

        scene.autoUpdate = false;
        scene.updateMatrixWorld();

        // 1) g-buffer normals + depth pass

        scene.traverse( setMaterialNormalDepth );

        // clear shared depth buffer

        this.renderer.autoClearDepth = true;
        this.renderer.autoClearStencil = true;

        // write 1 to shared stencil buffer
        // for non-background pixels

        //gl.enable( gl.STENCIL_TEST );
        gl.stencilOp( gl.REPLACE, gl.REPLACE, gl.REPLACE );
        gl.stencilFunc( gl.ALWAYS, 1, 0xffffffff );
        gl.clearStencil( 0 );

        compNormalDepth.render();

        // just touch foreground pixels (stencil == 1)
        // both in color and light passes

        gl.stencilFunc( gl.EQUAL, 1, 0xffffffff );
        gl.stencilOp( gl.KEEP, gl.KEEP, gl.KEEP );

        // 2) g-buffer color pass

        scene.traverse( setMaterialColor );

        // must use clean slate depth buffer
        // otherwise there are z-fighting glitches
        // not enough precision between two geometry passes
        // just to use EQUAL depth test

        this.renderer.autoClearDepth = true;
        this.renderer.autoClearStencil = false;

        compColor.render();

        // 3) light pass

        // do not clear depth buffer in this pass
        // depth from geometry pass is used for light culling
        // (write light proxy color pixel if behind scene pixel)

        this.renderer.autoClearDepth = false;

        scene.autoUpdate = true;

        gl.depthFunc( gl.GEQUAL );

        projectionMatrixInverse.getInverse( currentCamera.projectionMatrix );

        for ( var i = 0, il = lightSceneProxy.children.length; i < il; i ++ ) {

            var proxy = lightSceneProxy.children[ i ];
            updateLightProxy( proxy );

        }

        for ( var i = 0, il = lightSceneFullscreen.children.length; i < il; i ++ ) {

            var proxy = lightSceneFullscreen.children[ i ];
            updateLightProxy( proxy );

        }

        compLight.render();

        // 4) composite pass

        // return back to usual depth and stencil handling state

        this.renderer.autoClearDepth = true;
        this.renderer.autoClearStencil = true;
        gl.depthFunc( gl.LEQUAL );
        gl.disable( gl.STENCIL_TEST );

        compFinal.render( 0.1 );

    };

    //

    var createRenderTargets = function ( ) {

        var rtParamsFloatLinear = { minFilter: THREE.NearestFilter, magFilter: THREE.LinearFilter, stencilBuffer: true,
            format: THREE.RGBAFormat, type: THREE.FloatType };

        var rtParamsFloatNearest = { minFilter: THREE.NearestFilter, magFilter: THREE.NearestFilter, stencilBuffer: true,
            format: THREE.RGBAFormat, type: THREE.FloatType };

        var rtParamsUByte = { minFilter: THREE.NearestFilter, magFilter: THREE.LinearFilter, stencilBuffer: false,
            format: THREE.RGBFormat, type: THREE.UnsignedByteType };

        // g-buffers

        var rtColor   = new THREE.WebGLRenderTarget( scaledWidth, scaledHeight, rtParamsFloatNearest );
        var rtNormalDepth = new THREE.WebGLRenderTarget( scaledWidth, scaledHeight, rtParamsFloatNearest );
        var rtLight   = new THREE.WebGLRenderTarget( scaledWidth, scaledHeight, rtParamsFloatLinear );
        var rtFinal   = new THREE.WebGLRenderTarget( scaledWidth, scaledHeight, rtParamsUByte );

        rtColor.generateMipmaps = false;
        rtNormalDepth.generateMipmaps = false;
        rtLight.generateMipmaps = false;
        rtFinal.generateMipmaps = false;

        // normal + depth composer

        passNormalDepth = new THREE.RenderPass();
        passNormalDepth.clear = true;

        compNormalDepth = new THREE.EffectComposer( _this.renderer, rtNormalDepth );
        compNormalDepth.addPass( passNormalDepth );

        // color composer

        passColor = new THREE.RenderPass();
        passColor.clear = true;

        compColor = new THREE.EffectComposer( _this.renderer, rtColor );
        compColor.addPass( passColor );

        compColor.renderTarget2.shareDepthFrom = compNormalDepth.renderTarget2;

        // light composer

        passLightFullscreen = new THREE.RenderPass();
        passLightFullscreen.clear = true;

        passLightProxy = new THREE.RenderPass();
        passLightProxy.clear = false;

        compLight = new THREE.EffectComposer( _this.renderer, rtLight );
        compLight.addPass( passLightFullscreen );
        compLight.addPass( passLightProxy );

        compLight.renderTarget2.shareDepthFrom = compNormalDepth.renderTarget2;

        // final composer

        compositePass = new THREE.ShaderPass( compositeShader );
        compositePass.uniforms[ 'samplerLight' ].value = compLight.renderTarget2;
        compositePass.uniforms[ 'brightness' ].value = brightness;
        compositePass.material.blending = THREE.NoBlending;
        compositePass.clear = true;

        var defines;

        switch ( tonemapping ) {

            case THREE.SimpleOperator:    defines = { "TONEMAP_SIMPLE": true };    break;
            case THREE.LinearOperator:    defines = { "TONEMAP_LINEAR": true };    break;
            case THREE.ReinhardOperator:  defines = { "TONEMAP_REINHARD": true };  break;
            case THREE.FilmicOperator:    defines = { "TONEMAP_FILMIC": true };    break;
            case THREE.UnchartedOperator: defines = { "TONEMAP_UNCHARTED": true }; break;

        }

        compositePass.material.defines = defines;

        // FXAA

        effectFXAA = new THREE.ShaderPass( THREE.FXAAShader );
        effectFXAA.uniforms[ 'resolution' ].value.set( 1 / pixelWidth, 1 / pixelHeight );
        effectFXAA.renderToScreen = true;

        //

        compFinal = new THREE.EffectComposer( _this.renderer, rtFinal );
        compFinal.addPass( compositePass );
        compFinal.addPass( effectFXAA );

        if ( antialias ) {

            effectFXAA.enabled = true;
            compositePass.renderToScreen = false;

        } else {

            effectFXAA.enabled = false;
            compositePass.renderToScreen = true;

        }

    };

    // init

    createRenderTargets();

};

// tonemapping operator types

THREE.NoOperator = 0;
THREE.SimpleOperator = 1;
THREE.LinearOperator = 2;
THREE.ReinhardOperator = 3;
THREE.FilmicOperator = 4;
THREE.UnchartedOperator = 5;

/**
 * @author alteredq / http://alteredqualia.com/
 * @author MPanknin / http://www.redplant.de/
 * @author benaadams / http://blog.illyriad.co.uk/
 *
 */


var DeferredShaderChunk = {

    // decode float to vec3

    unpackFloat: [

        "vec3 float_to_vec3( float data ) {",

        "vec3 uncompressed;",
        "uncompressed.x = fract( data );",
        "float zInt = floor( data / 255.0 );",
        "uncompressed.z = fract( zInt / 255.0 );",
        "uncompressed.y = fract( floor( data - ( zInt * 255.0 ) ) / 255.0 );",
        "return uncompressed;",

        "}"

    ].join("\n"),

    computeVertexPositionVS: [

        "vec2 texCoord = gl_FragCoord.xy / vec2( viewWidth, viewHeight );",

        "vec4 normalDepth = texture2D( samplerNormalDepth, texCoord );",
        "float z = normalDepth.w;",

        "if ( z == 0.0 ) discard;",

        "vec2 xy = texCoord * 2.0 - 1.0;",

        "vec4 vertexPositionProjected = vec4( xy, z, 1.0 );",
        "vec4 vertexPositionVS = matProjInverse * vertexPositionProjected;",
        "vertexPositionVS.xyz /= vertexPositionVS.w;",
        "vertexPositionVS.w = 1.0;"

    ].join("\n"),

    computeNormal: [

        "vec3 normal = normalDepth.xyz * 2.0 - 1.0;"

    ].join("\n"),

    unpackColorMap: [

        "vec4 colorMap = texture2D( samplerColor, texCoord );",

        "vec3 albedo = float_to_vec3( abs( colorMap.x ) );",
        "vec3 specularColor = float_to_vec3( abs( colorMap.y ) );",
        "float shininess = abs( colorMap.z );",
        "float wrapAround = sign( colorMap.z );",
        "float additiveSpecular = sign( colorMap.y );"

    ].join("\n"),

    computeDiffuse: [

        "float dotProduct = dot( normal, lightVector );",
        "float diffuseFull = max( dotProduct, 0.0 );",

        "vec3 diffuse;",

        "if ( wrapAround < 0.0 ) {",

        // wrap around lighting

        "float diffuseHalf = max( 0.5 * dotProduct + 0.5, 0.0 );",

        "const vec3 wrapRGB = vec3( 1.0, 1.0, 1.0 );",
        "diffuse = mix( vec3( diffuseFull ), vec3( diffuseHalf ), wrapRGB );",

        "} else {",

        // simple lighting

        "diffuse = vec3( diffuseFull );",

        "}"

    ].join("\n"),

    computeSpecular: [

        "vec3 halfVector = normalize( lightVector - normalize( vertexPositionVS.xyz ) );",
        "float dotNormalHalf = max( dot( normal, halfVector ), 0.0 );",

        // simple specular

        //"vec3 specular = specularColor * max( pow( dotNormalHalf, shininess ), 0.0 ) * diffuse;",

        // physically based specular

        "float specularNormalization = ( shininess + 2.0001 ) / 8.0;",

        "vec3 schlick = specularColor + vec3( 1.0 - specularColor ) * pow( 1.0 - dot( lightVector, halfVector ), 5.0 );",
        "vec3 specular = schlick * max( pow( dotNormalHalf, shininess ), 0.0 ) * diffuse * specularNormalization;"

    ].join("\n"),

    combine: [

        "vec3 light = lightIntensity * lightColor;",
        "gl_FragColor = vec4( light * ( albedo * diffuse + specular ), attenuation );"

    ].join("\n")

};

THREE.ShaderDeferred = {

    "color" : {

        uniforms: THREE.UniformsUtils.merge( [

            THREE.UniformsLib[ "common" ],
            THREE.UniformsLib[ "fog" ],
            THREE.UniformsLib[ "shadowmap" ],

            {
                "emissive" :  { type: "c", value: new THREE.Color( 0x000000 ) },
                "specular" :  { type: "c", value: new THREE.Color( 0x111111 ) },
                "shininess":  { type: "f", value: 30 },
                "wrapAround": 		{ type: "f", value: 1 },
                "additiveSpecular": { type: "f", value: 1 },

                "samplerNormalDepth": { type: "t", value: null },
                "viewWidth": 		{ type: "f", value: 800 },
                "viewHeight": 		{ type: "f", value: 600 }
            }

        ] ),

        fragmentShader : [

            "uniform vec3 diffuse;",
            "uniform vec3 specular;",
            "uniform vec3 emissive;",
            "uniform float shininess;",
            "uniform float wrapAround;",
            "uniform float additiveSpecular;",

            THREE.ShaderChunk[ "color_pars_fragment" ],
            THREE.ShaderChunk[ "map_pars_fragment" ],
            THREE.ShaderChunk[ "lightmap_pars_fragment" ],

            "#ifdef USE_ENVMAP",

            "varying vec3 vWorldPosition;",

            "uniform float reflectivity;",
            "uniform samplerCube envMap;",
            "uniform float flipEnvMap;",
            "uniform int combine;",

            "uniform bool useRefract;",
            "uniform float refractionRatio;",

            "uniform sampler2D samplerNormalDepth;",
            "uniform float viewHeight;",
            "uniform float viewWidth;",

            "#endif",

            THREE.ShaderChunk[ "fog_pars_fragment" ],
            THREE.ShaderChunk[ "shadowmap_pars_fragment" ],
            THREE.ShaderChunk[ "specularmap_pars_fragment" ],

            "const float unit = 255.0/256.0;",

            "float vec3_to_float( vec3 data ) {",

            "highp float compressed = fract( data.x * unit ) + floor( data.y * unit * 255.0 ) + floor( data.z * unit * 255.0 ) * 255.0;",
            "return compressed;",

            "}",

            "void main() {",

            "const float opacity = 1.0;",

            "gl_FragColor = vec4( diffuse, opacity );",

            THREE.ShaderChunk[ "map_fragment" ],
            THREE.ShaderChunk[ "alphatest_fragment" ],
            THREE.ShaderChunk[ "specularmap_fragment" ],
            THREE.ShaderChunk[ "lightmap_fragment" ],
            THREE.ShaderChunk[ "color_fragment" ],

            "#ifdef USE_ENVMAP",

            "vec2 texCoord = gl_FragCoord.xy / vec2( viewWidth, viewHeight );",
            "vec4 normalDepth = texture2D( samplerNormalDepth, texCoord );",
            "vec3 normal = normalDepth.xyz * 2.0 - 1.0;",

            "vec3 reflectVec;",

            "vec3 cameraToVertex = normalize( vWorldPosition - cameraPosition );",

            "if ( useRefract ) {",

            "reflectVec = refract( cameraToVertex, normal, refractionRatio );",

            "} else { ",

            "reflectVec = reflect( cameraToVertex, normal );",

            "}",

            "#ifdef DOUBLE_SIDED",

            "float flipNormal = ( -1.0 + 2.0 * float( gl_FrontFacing ) );",
            "vec4 cubeColor = textureCube( envMap, flipNormal * vec3( flipEnvMap * reflectVec.x, reflectVec.yz ) );",

            "#else",

            "vec4 cubeColor = textureCube( envMap, vec3( flipEnvMap * reflectVec.x, reflectVec.yz ) );",

            "#endif",

            "#ifdef GAMMA_INPUT",

            "cubeColor.xyz *= cubeColor.xyz;",

            "#endif",

            "if ( combine == 1 ) {",

            "gl_FragColor.xyz = mix( gl_FragColor.xyz, cubeColor.xyz, specularStrength * reflectivity );",

            "} else if ( combine == 2 ) {",

            "gl_FragColor.xyz += cubeColor.xyz * specularStrength * reflectivity;",

            "} else {",

            "gl_FragColor.xyz = mix( gl_FragColor.xyz, gl_FragColor.xyz * cubeColor.xyz, specularStrength * reflectivity );",

            "}",

            "#endif",

            THREE.ShaderChunk[ "shadowmap_fragment" ],
            THREE.ShaderChunk[ "fog_fragment" ],

            //

            "const float compressionScale = 0.999;",

            //

            "vec3 diffuseMapColor;",

            "#ifdef USE_MAP",

            "diffuseMapColor = texelColor.xyz;",

            "#else",

            "diffuseMapColor = vec3( 1.0 );",

            "#endif",

            // diffuse color

            "gl_FragColor.x = vec3_to_float( compressionScale * gl_FragColor.xyz );",

            // specular color

            "if ( additiveSpecular < 0.0 ) {",

            "gl_FragColor.y = vec3_to_float( compressionScale * specular );",

            "} else {",

            "gl_FragColor.y = vec3_to_float( compressionScale * specular * diffuseMapColor );",

            "}",

            "gl_FragColor.y *= additiveSpecular;",

            // shininess

            "gl_FragColor.z = wrapAround * shininess;",

            // emissive color

            "#ifdef USE_COLOR",

            "gl_FragColor.w = vec3_to_float( compressionScale * emissive * diffuseMapColor * vColor );",

            "#else",

            "gl_FragColor.w = vec3_to_float( compressionScale * emissive * diffuseMapColor );",

            "#endif",

            "}"

        ].join("\n"),

        vertexShader : [

            THREE.ShaderChunk[ "map_pars_vertex" ],
            THREE.ShaderChunk[ "lightmap_pars_vertex" ],
            THREE.ShaderChunk[ "color_pars_vertex" ],
            THREE.ShaderChunk[ "morphtarget_pars_vertex" ],
            THREE.ShaderChunk[ "skinning_pars_vertex" ],
            THREE.ShaderChunk[ "shadowmap_pars_vertex" ],

            "#ifdef USE_ENVMAP",

            "varying vec3 vWorldPosition;",

            "#endif",

            "void main() {",

            THREE.ShaderChunk[ "map_vertex" ],
            THREE.ShaderChunk[ "lightmap_vertex" ],
            THREE.ShaderChunk[ "color_vertex" ],

            THREE.ShaderChunk[ "skinbase_vertex" ],

            THREE.ShaderChunk[ "morphtarget_vertex" ],
            THREE.ShaderChunk[ "skinning_vertex" ],
            THREE.ShaderChunk[ "default_vertex" ],

            THREE.ShaderChunk[ "worldpos_vertex" ],
            THREE.ShaderChunk[ "shadowmap_vertex" ],

            "#ifdef USE_ENVMAP",

            "vWorldPosition = worldPosition.xyz;",

            "#endif",

            "}"

        ].join("\n")

    },

    "normalDepth" : {

        uniforms: {

            bumpMap: 	  { type: "t", value: null },
            bumpScale:	  { type: "f", value: 1 },
            offsetRepeat: { type: "v4", value: new THREE.Vector4( 0, 0, 1, 1 ) }

        },

        fragmentShader : [

            "#ifdef USE_BUMPMAP",

            "#extension GL_OES_standard_derivatives : enable\n",

            "varying vec2 vUv;",
            "varying vec3 vViewPosition;",

            THREE.ShaderChunk[ "bumpmap_pars_fragment" ],

            "#endif",

            "varying vec3 normalView;",
            "varying vec4 clipPos;",

            "void main() {",

            "vec3 normal = normalize( normalView );",

            "#ifdef USE_BUMPMAP",

            "normal = perturbNormalArb( -vViewPosition, normal, dHdxy_fwd() );",

            "#endif",

            "gl_FragColor.xyz = normal * 0.5 + 0.5;",
            "gl_FragColor.w = clipPos.z / clipPos.w;",

            "}"

        ].join("\n"),

        vertexShader : [

            "varying vec3 normalView;",
            "varying vec4 clipPos;",

            "#ifdef USE_BUMPMAP",

            "varying vec2 vUv;",
            "varying vec3 vViewPosition;",

            "uniform vec4 offsetRepeat;",

            "#endif",

            THREE.ShaderChunk[ "morphtarget_pars_vertex" ],
            THREE.ShaderChunk[ "skinning_pars_vertex" ],

            "void main() {",

            THREE.ShaderChunk[ "morphnormal_vertex" ],
            THREE.ShaderChunk[ "skinbase_vertex" ],
            THREE.ShaderChunk[ "skinnormal_vertex" ],
            THREE.ShaderChunk[ "defaultnormal_vertex" ],

            THREE.ShaderChunk[ "morphtarget_vertex" ],
            THREE.ShaderChunk[ "skinning_vertex" ],
            THREE.ShaderChunk[ "default_vertex" ],

            "normalView = normalize( normalMatrix * objectNormal );",

            "#ifdef USE_BUMPMAP",

            "vUv = uv * offsetRepeat.zw + offsetRepeat.xy;",
            "vViewPosition = -mvPosition.xyz;",

            "#endif",

            "clipPos = gl_Position;",

            "}"

        ].join("\n")

    },

    "composite" : {

        uniforms: {

            samplerLight: 	{ type: "t", value: null },
            brightness:		{ type: "f", value: 1 }

        },

        fragmentShader : [

            "varying vec2 texCoord;",
            "uniform sampler2D samplerLight;",
            "uniform float brightness;",

            // tonemapping operators
            // based on John Hable's HLSL snippets
            // from http://filmicgames.com/archives/75

            "#ifdef TONEMAP_UNCHARTED",

            "const float A = 0.15;",
            "const float B = 0.50;",
            "const float C = 0.10;",
            "const float D = 0.20;",
            "const float E = 0.02;",
            "const float F = 0.30;",
            "const float W = 11.2;",

            "vec3 Uncharted2Tonemap( vec3 x ) {",

            "return ( ( x * ( A * x + C * B ) + D * E ) / ( x * ( A * x + B ) + D * F ) ) - E / F;",

            "}",

            "#endif",

            "void main() {",

            "vec3 inColor = texture2D( samplerLight, texCoord ).xyz;",
            "inColor *= brightness;",

            "vec3 outColor;",

            "#if defined( TONEMAP_SIMPLE )",

            "outColor = sqrt( inColor );",

            "#elif defined( TONEMAP_LINEAR )",

            // simple linear to gamma conversion

            "outColor = pow( inColor, vec3( 1.0 / 2.2 ) );",

            "#elif defined( TONEMAP_REINHARD )",

            // Reinhard operator

            "inColor = inColor / ( 1.0 + inColor );",
            "outColor = pow( inColor, vec3( 1.0 / 2.2 ) );",

            "#elif defined( TONEMAP_FILMIC )",

            // filmic operator by Jim Hejl and Richard Burgess-Dawson

            "vec3 x = max( vec3( 0.0 ), inColor - 0.004 );",
            "outColor = ( x * ( 6.2 * x + 0.5 ) ) / ( x * ( 6.2 * x + 1.7 ) + 0.06 );",

            "#elif defined( TONEMAP_UNCHARTED )",

            // tonemapping operator from Uncharted 2 by John Hable

            "float ExposureBias = 2.0;",
            "vec3 curr = Uncharted2Tonemap( ExposureBias * inColor );",

            "vec3 whiteScale = vec3( 1.0 ) / Uncharted2Tonemap( vec3( W ) );",
            "vec3 color = curr * whiteScale;",

            "outColor = pow( color, vec3( 1.0 / 2.2 ) );",

            "#else",

            "outColor = inColor;",

            "#endif",

            "gl_FragColor = vec4( outColor, 1.0 );",

            "}"

        ].join("\n"),

        vertexShader : [

            "varying vec2 texCoord;",

            "void main() {",

            "vec4 pos = vec4( sign( position.xy ), 0.0, 1.0 );",
            "texCoord = pos.xy * vec2( 0.5 ) + 0.5;",
            "gl_Position = pos;",

            "}"

        ].join("\n")

    },

    "pointLight" : {

        uniforms: {

            samplerNormalDepth: { type: "t", value: null },
            samplerColor: 		{ type: "t", value: null },
            matProjInverse: { type: "m4", value: new THREE.Matrix4() },
            viewWidth: 		{ type: "f", value: 800 },
            viewHeight: 	{ type: "f", value: 600 },

            lightPositionVS:{ type: "v3", value: new THREE.Vector3( 0, 0, 0 ) },
            lightColor: 	{ type: "c", value: new THREE.Color( 0x000000 ) },
            lightIntensity: { type: "f", value: 1.0 },
            lightRadius: 	{ type: "f", value: 1.0 }

        },

        fragmentShader : [

            "uniform sampler2D samplerColor;",
            "uniform sampler2D samplerNormalDepth;",

            "uniform float lightRadius;",
            "uniform float lightIntensity;",
            "uniform float viewHeight;",
            "uniform float viewWidth;",

            "uniform vec3 lightColor;",
            "uniform vec3 lightPositionVS;",

            "uniform mat4 matProjInverse;",

            THREE.DeferredShaderChunk[ "unpackFloat" ],

            "void main() {",

            THREE.DeferredShaderChunk[ "computeVertexPositionVS" ],

            // bail out early when pixel outside of light sphere

            "vec3 lightVector = lightPositionVS - vertexPositionVS.xyz;",
            "float distance = length( lightVector );",

            "if ( distance > lightRadius ) discard;",

            THREE.DeferredShaderChunk[ "computeNormal" ],
            THREE.DeferredShaderChunk[ "unpackColorMap" ],

            // compute light

            "lightVector = normalize( lightVector );",

            THREE.DeferredShaderChunk[ "computeDiffuse" ],
            THREE.DeferredShaderChunk[ "computeSpecular" ],

            // combine

            "float cutoff = 0.3;",
            "float denom = distance / lightRadius + 1.0;",
            "float attenuation = 1.0 / ( denom * denom );",
            "attenuation = ( attenuation - cutoff ) / ( 1.0 - cutoff );",
            "attenuation = max( attenuation, 0.0 );",
            "attenuation *= attenuation;",

            THREE.DeferredShaderChunk[ "combine" ],

            "}"

        ].join("\n"),

        vertexShader : [

            "void main() { ",

            // sphere proxy needs real position

            "vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );",
            "gl_Position = projectionMatrix * mvPosition;",

            "}"

        ].join("\n")

    },

    "spotLight" : {

        uniforms: {

            samplerNormalDepth: { type: "t", value: null },
            samplerColor: 		{ type: "t", value: null },
            matProjInverse: { type: "m4", value: new THREE.Matrix4() },
            viewWidth: 		{ type: "f", value: 800 },
            viewHeight: 	{ type: "f", value: 600 },

            lightPositionVS :{ type: "v3", value: new THREE.Vector3( 0, 1, 0 ) },
            lightDirectionVS:{ type: "v3", value: new THREE.Vector3( 0, 1, 0 ) },
            lightColor: 	{ type: "c", value: new THREE.Color( 0x000000 ) },
            lightIntensity: { type: "f", value: 1.0 },
            lightDistance: 	{ type: "f", value: 1.0 },
            lightAngle: 	{ type: "f", value: 1.0 }

        },

        fragmentShader : [

            "uniform vec3 lightPositionVS;",
            "uniform vec3 lightDirectionVS;",

            "uniform sampler2D samplerColor;",
            "uniform sampler2D samplerNormalDepth;",

            "uniform float viewHeight;",
            "uniform float viewWidth;",

            "uniform float lightAngle;",
            "uniform float lightIntensity;",
            "uniform vec3 lightColor;",

            "uniform mat4 matProjInverse;",

            THREE.DeferredShaderChunk[ "unpackFloat" ],

            "void main() {",

            THREE.DeferredShaderChunk[ "computeVertexPositionVS" ],
            THREE.DeferredShaderChunk[ "computeNormal" ],
            THREE.DeferredShaderChunk[ "unpackColorMap" ],

            // compute light

            "vec3 lightVector = normalize( lightPositionVS.xyz - vertexPositionVS.xyz );",

            "float rho = dot( lightDirectionVS, lightVector );",
            "float rhoMax = cos( lightAngle * 0.5 );",

            "if ( rho <= rhoMax ) discard;",

            "float theta = rhoMax + 0.0001;",
            "float phi = rhoMax + 0.05;",
            "float falloff = 4.0;",

            "float spot = 0.0;",

            "if ( rho >= phi ) {",

            "spot = 1.0;",

            "} else if ( rho <= theta ) {",

            "spot = 0.0;",

            "} else { ",

            "spot = pow( ( rho - theta ) / ( phi - theta ), falloff );",

            "}",

            THREE.DeferredShaderChunk[ "computeDiffuse" ],

            "diffuse *= spot;",

            THREE.DeferredShaderChunk[ "computeSpecular" ],

            // combine

            "const float attenuation = 1.0;",

            THREE.DeferredShaderChunk[ "combine" ],

            "}"

        ].join("\n"),

        vertexShader : [

            "void main() { ",

            // full screen quad proxy

            "gl_Position = vec4( sign( position.xy ), 0.0, 1.0 );",

            "}"

        ].join("\n")

    },

    "directionalLight" : {

        uniforms: {

            samplerNormalDepth: { type: "t", value: null },
            samplerColor: 		{ type: "t", value: null },
            matProjInverse: { type: "m4", value: new THREE.Matrix4() },
            viewWidth: 		{ type: "f", value: 800 },
            viewHeight: 	{ type: "f", value: 600 },

            lightDirectionVS: { type: "v3", value: new THREE.Vector3( 0, 1, 0 ) },
            lightColor: 	{ type: "c", value: new THREE.Color( 0x000000 ) },
            lightIntensity: { type: "f", value: 1.0 }

        },

        fragmentShader : [

            "uniform sampler2D samplerColor;",
            "uniform sampler2D samplerNormalDepth;",

            "uniform float lightRadius;",
            "uniform float lightIntensity;",
            "uniform float viewHeight;",
            "uniform float viewWidth;",

            "uniform vec3 lightColor;",
            "uniform vec3 lightDirectionVS;",

            "uniform mat4 matProjInverse;",

            THREE.DeferredShaderChunk[ "unpackFloat" ],

            "void main() {",

            THREE.DeferredShaderChunk[ "computeVertexPositionVS" ],
            THREE.DeferredShaderChunk[ "computeNormal" ],
            THREE.DeferredShaderChunk[ "unpackColorMap" ],

            // compute light

            "vec3 lightVector = lightDirectionVS;",

            THREE.DeferredShaderChunk[ "computeDiffuse" ],
            THREE.DeferredShaderChunk[ "computeSpecular" ],

            // combine

            "const float attenuation = 1.0;",

            THREE.DeferredShaderChunk[ "combine" ],

            "}"

        ].join("\n"),

        vertexShader : [

            "void main() { ",

            // full screen quad proxy

            "gl_Position = vec4( sign( position.xy ), 0.0, 1.0 );",

            "}"

        ].join("\n")

    },

    "hemisphereLight" : {

        uniforms: {

            samplerNormalDepth: { type: "t", value: null },
            samplerColor: 		{ type: "t", value: null },
            matProjInverse: { type: "m4", value: new THREE.Matrix4() },
            viewWidth: 		{ type: "f", value: 800 },
            viewHeight: 	{ type: "f", value: 600 },

            lightDirectionVS: { type: "v3", value: new THREE.Vector3( 0, 1, 0 ) },
            lightColorSky: 	  { type: "c", value: new THREE.Color( 0x000000 ) },
            lightColorGround: { type: "c", value: new THREE.Color( 0x000000 ) },
            lightIntensity:   { type: "f", value: 1.0 }

        },

        fragmentShader : [

            "uniform sampler2D samplerColor;",
            "uniform sampler2D samplerNormalDepth;",

            "uniform float lightRadius;",
            "uniform float lightIntensity;",
            "uniform float viewHeight;",
            "uniform float viewWidth;",

            "uniform vec3 lightColorSky;",
            "uniform vec3 lightColorGround;",
            "uniform vec3 lightDirectionVS;",

            "uniform mat4 matProjInverse;",

            THREE.DeferredShaderChunk[ "unpackFloat" ],

            "void main() {",

            THREE.DeferredShaderChunk[ "computeVertexPositionVS" ],
            THREE.DeferredShaderChunk[ "computeNormal" ],
            THREE.DeferredShaderChunk[ "unpackColorMap" ],

            // compute light

            "vec3 lightVector = lightDirectionVS;",

            // diffuse

            "float dotProduct = dot( normal, lightVector );",
            "float hemiDiffuseWeight = 0.5 * dotProduct + 0.5;",

            "vec3 hemiColor = mix( lightColorGround, lightColorSky, hemiDiffuseWeight );",

            "vec3 diffuse = hemiColor;",

            // specular (sky light)

            "vec3 hemiHalfVectorSky = normalize( lightVector - vertexPositionVS.xyz );",
            "float hemiDotNormalHalfSky = 0.5 * dot( normal, hemiHalfVectorSky ) + 0.5;",
            "float hemiSpecularWeightSky = max( pow( hemiDotNormalHalfSky, shininess ), 0.0 );",

            // specular (ground light)

            "vec3 lVectorGround = -lightVector;",

            "vec3 hemiHalfVectorGround = normalize( lVectorGround - vertexPositionVS.xyz );",
            "float hemiDotNormalHalfGround = 0.5 * dot( normal, hemiHalfVectorGround ) + 0.5;",
            "float hemiSpecularWeightGround = max( pow( hemiDotNormalHalfGround, shininess ), 0.0 );",

            "float dotProductGround = dot( normal, lVectorGround );",

            "float specularNormalization = ( shininess + 2.0001 ) / 8.0;",

            "vec3 schlickSky = specularColor + vec3( 1.0 - specularColor ) * pow( 1.0 - dot( lightVector, hemiHalfVectorSky ), 5.0 );",
            "vec3 schlickGround = specularColor + vec3( 1.0 - specularColor ) * pow( 1.0 - dot( lVectorGround, hemiHalfVectorGround ), 5.0 );",
            "vec3 specular = hemiColor * specularNormalization * ( schlickSky * hemiSpecularWeightSky * max( dotProduct, 0.0 ) + schlickGround * hemiSpecularWeightGround * max( dotProductGround, 0.0 ) );",

            // combine

            "gl_FragColor = vec4( lightIntensity * ( albedo * diffuse + specular ), 1.0 );",

            "}"

        ].join("\n"),

        vertexShader : [

            "void main() { ",

            // full screen quad proxy

            "gl_Position = vec4( sign( position.xy ), 0.0, 1.0 );",

            "}"

        ].join("\n")

    },

    "areaLight" : {

        uniforms: {

            samplerNormalDepth: { type: "t", value: null },
            samplerColor: 		{ type: "t", value: null },
            matProjInverse: { type: "m4", value: new THREE.Matrix4() },
            viewWidth: 		{ type: "f", value: 800 },
            viewHeight: 	{ type: "f", value: 600 },

            lightPositionVS: { type: "v3", value: new THREE.Vector3( 0, 1, 0 ) },
            lightNormalVS: 	 { type: "v3", value: new THREE.Vector3( 0, -1, 0 ) },
            lightRightVS:  	 { type: "v3", value: new THREE.Vector3( 1, 0, 0 ) },
            lightUpVS:  	 { type: "v3", value: new THREE.Vector3( 1, 0, 0 ) },

            lightColor: 	{ type: "c", value: new THREE.Color( 0x000000 ) },
            lightIntensity: { type: "f", value: 1.0 },

            lightWidth:  { type: "f", value: 1.0 },
            lightHeight: { type: "f", value: 1.0 },

            constantAttenuation:  { type: "f", value: 1.5 },
            linearAttenuation:    { type: "f", value: 0.5 },
            quadraticAttenuation: { type: "f", value: 0.1 }

        },

        fragmentShader : [

            "uniform vec3 lightPositionVS;",
            "uniform vec3 lightNormalVS;",
            "uniform vec3 lightRightVS;",
            "uniform vec3 lightUpVS;",

            "uniform sampler2D samplerColor;",
            "uniform sampler2D samplerNormalDepth;",

            "uniform float lightWidth;",
            "uniform float lightHeight;",

            "uniform float constantAttenuation;",
            "uniform float linearAttenuation;",
            "uniform float quadraticAttenuation;",

            "uniform float lightIntensity;",
            "uniform vec3 lightColor;",

            "uniform float viewHeight;",
            "uniform float viewWidth;",

            "uniform mat4 matProjInverse;",

            THREE.DeferredShaderChunk[ "unpackFloat" ],

            "vec3 projectOnPlane( vec3 point, vec3 planeCenter, vec3 planeNorm ) {",

            "return point - dot( point - planeCenter, planeNorm ) * planeNorm;",

            "}",

            "bool sideOfPlane( vec3 point, vec3 planeCenter, vec3 planeNorm ) {",

            "return ( dot( point - planeCenter, planeNorm ) >= 0.0 );",

            "}",

            "vec3 linePlaneIntersect( vec3 lp, vec3 lv, vec3 pc, vec3 pn ) {",

            "return lp + lv * ( dot( pn, pc - lp ) / dot( pn, lv ) );",

            "}",

            "float calculateAttenuation( float dist ) {",

            "return ( 1.0 / ( constantAttenuation + linearAttenuation * dist + quadraticAttenuation * dist * dist ) );",

            "}",

            "void main() {",

            THREE.DeferredShaderChunk[ "computeVertexPositionVS" ],
            THREE.DeferredShaderChunk[ "computeNormal" ],
            THREE.DeferredShaderChunk[ "unpackColorMap" ],

            "float w = lightWidth;",
            "float h = lightHeight;",

            "vec3 proj = projectOnPlane( vertexPositionVS.xyz, lightPositionVS, lightNormalVS );",
            "vec3 dir = proj - lightPositionVS;",

            "vec2 diagonal = vec2( dot( dir, lightRightVS ), dot( dir, lightUpVS ) );",
            "vec2 nearest2D = vec2( clamp( diagonal.x, -w, w ), clamp( diagonal.y, -h, h ) );",
            "vec3 nearestPointInside = vec3( lightPositionVS ) + ( lightRightVS * nearest2D.x + lightUpVS * nearest2D.y );",

            "vec3 lightDir = normalize( nearestPointInside - vertexPositionVS.xyz );",
            "float NdotL = max( dot( lightNormalVS, -lightDir ), 0.0 );",
            "float NdotL2 = max( dot( normal, lightDir ), 0.0 );",

            //"if ( NdotL2 * NdotL > 0.0 && sideOfPlane( vertexPositionVS.xyz, lightPositionVS, lightNormalVS ) ) {",
            "if ( NdotL2 * NdotL > 0.0 ) {",

            // diffuse

            "vec3 diffuse = vec3( sqrt( NdotL * NdotL2 ) );",

            // specular

            "vec3 specular = vec3( 0.0 );",

            "vec3 R = reflect( normalize( -vertexPositionVS.xyz ), normal );",
            "vec3 E = linePlaneIntersect( vertexPositionVS.xyz, R, vec3( lightPositionVS ), lightNormalVS );",

            "float specAngle = dot( R, lightNormalVS );",

            "if ( specAngle > 0.0 ) {",

            "vec3 dirSpec = E - vec3( lightPositionVS );",
            "vec2 dirSpec2D = vec2( dot( dirSpec, lightRightVS ), dot( dirSpec, lightUpVS ) );",
            "vec2 nearestSpec2D = vec2( clamp( dirSpec2D.x, -w, w ), clamp( dirSpec2D.y, -h, h ) );",
            "float specFactor = 1.0 - clamp( length( nearestSpec2D - dirSpec2D ) * 0.05 * shininess, 0.0, 1.0 );",
            "specular = specularColor * specFactor * specAngle * diffuse;",

            "}",

            // combine

            "float dist = distance( vertexPositionVS.xyz, nearestPointInside );",
            "float attenuation = calculateAttenuation( dist );",

            THREE.DeferredShaderChunk[ "combine" ],

            "} else {",

            "discard;",

            "}",

            "}"

        ].join("\n"),

        vertexShader : [

            "void main() {",

            // full screen quad proxy

            "gl_Position = vec4( sign( position.xy ), 0.0, 1.0 );",

            "}"

        ].join("\n")

    },

    "emissiveLight" : {

        uniforms: {

            samplerColor: 	{ type: "t", value: null },
            viewWidth: 		{ type: "f", value: 800 },
            viewHeight: 	{ type: "f", value: 600 },

        },

        fragmentShader : [

            "uniform sampler2D samplerColor;",

            "uniform float viewHeight;",
            "uniform float viewWidth;",

            THREE.DeferredShaderChunk[ "unpackFloat" ],

            "void main() {",

            "vec2 texCoord = gl_FragCoord.xy / vec2( viewWidth, viewHeight );",

            "vec4 colorMap = texture2D( samplerColor, texCoord );",
            "vec3 emissiveColor = float_to_vec3( abs( colorMap.w ) );",

            "gl_FragColor = vec4( emissiveColor, 1.0 );",

            "}"

        ].join("\n"),

        vertexShader : [

            "void main() { ",

            // full screen quad proxy

            "gl_Position = vec4( sign( position.xy ), 0.0, 1.0 );",

            "}"

        ].join("\n")

    }

};

module.exports = {
    WebGLDeferredRenderer,
    DeferredShaderChunk
};