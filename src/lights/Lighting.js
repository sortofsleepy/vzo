/**
 * Basic lighting array
 */
import {WebGLDeferredRenderer,DeferredShaderChunk} from '../DeferedRenderer.js'
class Lighting {
    constructor(numlights,scene,options){
        this.defaults = {
            intensity:1.5,
            distance:30,
            color:0xffffff
        };

        options = options !== undefined ? this._extendOptions(options) : this.defaults

        this.num = numlights;
        this.lights = [];

        var geo = new THREE.SphereGeometry(0.5,16,9);

        for(var i = 0;i<numlights;++i){
            var light = new THREE.PointLight(0xff0040,options.intensity,distance);
            var mesh = new THREE.Mesh(geo,new THREE.MeshBasicMaterial({
                color:options.color,
                opacity:0.4
            }));

            light.add(mesh);
            this.lights.push(light);
        }

    }

    //combine default and user passed in options.
    _extendOptions(user){
        for(var i in user){
            this.defaults[i] = user[i];
        }
    }
}

class DeferredLighting extends Lighting{
    constructor(numlights,scene,options){
        super(numlights,scene,options);


        //////// INIT LIGHTS ////////////
        var distance = 40;
        var light = new THREE.PointLight(0xffffff,options.intensity,1.5 * distance);
        scene.add(light);
        lights.push(light);

        var c = new THREE.Vector3();

        for(var i = 0;i<numLights;++i){
            var light = new THREE.PointLight(0xffffff,2.0,distance);
            c.set(Math.random(),Math.random(),Math.random()).normalize();
            light.color.setRGB(c.x,c.y,c.z);
            scene.add(light);
            lights.push(light);
        }

        var geometry = new THREE.SphereGeometry(0.7,7,7);
        for(var i = 0;i<numLights;++i){
            var light = lights[i];
            var mat = new THREE.MeshBasicMaterial();
            material.color = light.color;

            var emitter = new THREE.Mesh(geometry,mat);
            light.add(emitter);
        }

        this.renderTarget = new THREE.WebGLRenderTarget({
            minFilter: THREE.NearestFilter,
            magFilter: THREE.NearestFilter,
            format: THREE.RGBAFormat,
            type:THREE.FloatType,
            stencilBuffer: false
        });

    }

}

export default {
    LightArray:Lighting,
    DeferredLightArray:DeferredLighting
}