/**
 * A class for performing GPU animations.
 *
 * Mostly based off of the work of Cabbibo and his
 * PhysicsRenderer for Three.js
 * https://github.com/cabbibo/PhysicsRenderer/
 */
class Composer extends THREE.Geometry {
    constructor(size,renderer,options){
        this._checkCapabilities(renderer);

        //options for the buffering
        var defaults = {
            width:size,
            height:size,
            wrapS:THREE.RepeatWrapping,
            wrapT:THREE.RepeatWrapping,
            minFilter:THREE.NearestFilter,
            magFilter:THREE.NearestFilter,
            format:THREE.RGBAFormat,
            stencilBuffer:false
        };
        this.options = options !== undefined ? options : defaults;

        if(options !== undefined){
            this._extendOptions(defaults);
        }

        this.size = size;

        this.s2 = size * size;

        this.passes = [];

        this.passThru = this.passShader = this._createPassthru();

        this.renderer = renderer;
        this.camera = new THREE.OrthographicCamera( - 0.5, 0.5, 0.5, - 0.5, 0, 1 );
        this.scene = new THREE.Scene();
        this.mesh = new THREE.Mesh( new THREE.PlaneBufferGeometry( 1, 1 ) );
        this.scene.add( this.mesh );
        this.counter = 0;
        this.clock = new THREE.Clock();
        this.resolution = new THREE.Vector2(this.size,this.size);

        window.addEventListener("resize",this.windowResize);
        this.windowRes = new THREE.Vector2(window.innerWidth,window.innerHeight);


        //original uniforms
        this.uniforms = {
            t_oPos:{
                type:"t"  ,
                value:null
            },
            t_pos:{
                type:"t"  ,
                value:null
            },
            resolution: {
                type:"v2" ,
                value: this.resolution
            },
            screenResolution :{
                type:'v2',
                value:this.windowRes
            }
        };

        this.numPasses = 0;

    }

    /**
     * Update any params affected by window resizing here.
     * @param e
     */
    windowResize(e){

        this.windowRes.x = window.innerWidth;
        this.windowRes.y = window.innerHeight;

        //update the key uniforms like resolution on window resize
        for(var i = 0;i<this.passes.length;++i){
            this.passes[i].uniforms.screenResolution.value = this.windowRes;
        }
    }

    /**
     * Adds a pass for calculatn things
     * @param name name for the pass
     * @param source fragment source for the pas
     * @param uniforms uniforms related to dealing with the pass.
     */
    addPass(name,source,uniforms){
        this.simulationUniforms = {
            t_oPos:{
                type:"t"  ,
                value:null
            },
            t_pos:{
                type:"t"  ,
                value:null
            },
            resolution: {
                type:"v2" ,
                value: this.resolution
            },
            screenResolution :{
                type:'v2',
                value:this.windowRes
            }
        };

        for(var i in this.uniforms){
            uniforms[i] = this.uniforms[i]
        }

        //adds a new simulation pass.
        var program = new THREE.ShaderMaterial({
            uniforms:uniforms,
            vertexShader:this.passThruVS,
            fragmentShader:source
        });


        console.log(uniforms)
        var pass = {
            name:name,
            shader:program,
            buffers:[
                this._generateRenderTargets(),
                this._generateRenderTargets(),
                this._generateRenderTargets()
            ],
            uniforms:uniforms
        };

        pass["rt_1"] = pass.buffers[0];
        pass["rt_2"] = pass.buffers[1];
        pass["rt_3"] = pass.buffers[2];

        this._resetRand(30,pass);

        this.passes.push(pass);

        this.numPasses = this.passes.length;

        this.uniforms = uniforms;
    }

    update(){
        for(var i = 0;i<this.numPasses;++i){
            this.passes[i].uniforms.dT.value = performance.now() / 2 * 0.00009;
            var flipFlop = this.counter % 3;

            if( flipFlop == 0 ){

                this.passes[i].uniforms.t_oPos.value = this.passes[i].rt_1;
                this.passes[i].uniforms.t_pos.value = this.passes[i].rt_2;

                this._runPass( this.passes[i].shader, this.passes[i].rt_3 );



            }else if( flipFlop == 1 ){

                this.passes[i].uniforms.t_oPos.value = this.passes[i].rt_2;
                this.passes[i].uniforms.t_pos.value = this.passes[i].rt_3;

                this._runPass( this.passes[i].shader , this.passes[i].rt_1 );




            }else if( flipFlop == 2 ){

                this.passes[i].uniforms.t_oPos.value = this.passes[i].rt_3;
                this.passes[i].uniforms.t_pos.value = this.passes[i].rt_1;

                this._runPass( this.passes[i].shader , this.passes[i].rt_2 );


            }

            this.counter ++;

        }
    }


    /**================ INTERNAL FUNCTIONS =======================*/
    /**
     * If the options aren't undefined, apply the defaults to fill in the gaps
     * @param defaults the default options.
     * @private
     */
    _extendOptions(defaults){
        for(var i in defaults){
            if(this.options[i] === undefined){
                this.options[i] = defaults[i];
            }
        }
    }

    /**
     * Makes sure that the hardware is capable of doing GPU calculations
     * @param renderer
     */
    _checkCapabilities(renderer){
        var gl = renderer.context;

        if ( gl.getExtension( "OES_texture_float" ) === null ) {
            alert( "No Float Textures");
            return;
        }

        if ( gl.getParameter( gl.MAX_VERTEX_TEXTURE_IMAGE_UNITS ) === 0 ) {
            alert( "Vert Shader Textures don't work");
            return;
        }
    }
    /**
     * Create passthru shader
     * @returns {THREE.ShaderMaterial}
     */
    _createPassthru(){

        this.passThruVS =[
            "varying vec2 vUv;",
            "void main() {",
            "  vUv = uv;",
            "  gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );",
            "}"
        ].join("\n");

        this.passThruFS = [
            "uniform sampler2D texture;",
            "varying vec2 vUv;",
            "void main() {",
            "  vec2 read = vUv;",
            "  vec4 c = texture2D( texture , vUv );",
            "  gl_FragColor = c ;",
            //   "gl_FragColor = vec4(1.);",
            "}"
        ].join("\n");

        var uniforms = {
            texture:{  type:"t"  , value:null }
        };

        var texturePassShader = new THREE.ShaderMaterial({
            uniforms:uniforms,
            vertexShader:this.passThruVS,
            fragmentShader:this.passThruFS
        });

        return texturePassShader;
    }

    /**
     * Runs through a pass
     * @param shader
     * @param target
     */
    _runPass(shader,target){
        this.mesh.material = shader;
        this.renderer.render( this.scene, this.camera, target, false );
    }

    /**
     * Generates a render target
     * @param data any data that might want to get applied onto the target
     * @returns {THREE.WebGLRenderTarget}
     * @private
     */
    _generateRenderTargets(data){
        var options = this.options;

        var target = new THREE.WebGLRenderTarget(this.size,this.size,{
            minFilter: THREE.NearestFilter,
            magFilter: THREE.NearestFilter,
            format: THREE.RGBAFormat,
            type:THREE.FloatType,
            stencilBuffer: false
        });


        return target;
    }


    /**
     * Generates a data texture.
     * @param data optional precalculated data
     * @returns {THREE.DataTexture} returns a DataTexture
     * @private
     */
    _generateDataTexture(data){
        var a = null;

        if(data !== undefined){
            a = data;
        }else{

            a = new Float32Array( this.options.width * this.options.height * 3 );
            for ( var k = 0, kl = a.length; k < kl; k += 3 ) {

                var x = Math.random() - 0.5;
                var y = Math.random() - 0.5;
                var z = Math.random() - 0.5;

                a[ k + 0 ] = x * 10;
                a[ k + 1 ] = y * 10;
                a[ k + 2 ] = z * 10;

            }
        }


        var texture = new THREE.DataTexture( a, this.options.width,this.options.height, THREE.RGBFormat, THREE.FloatType );
        texture.minFilter = THREE.NearestFilter;
        texture.magFilter = THREE.NearestFilter;
        texture.needsUpdate = true;
        texture.flipY = false;

        return texture;
    }

    _reset(texture,pass){

        this.texture = texture;
        this.passThru.uniforms.texture.value = texture;

        this._runPass( this.passThru, pass.buffers[0] );
        this._runPass( this.passThru, pass.buffers[1]);
        this._runPass( this.passThru, pass.buffers[2] );
    }

    _resetRand(size,pass,alpha){
        var size = size || 100;
        var data = new Float32Array( this.s2 * 4 );

        for( var i =0; i < data.length; i++ ){

            //console.log('ss');
            data[ i ] = (Math.random() - .5 ) * size;

            if( alpha && i % 4 ===3 ){
                data[i] = 0;
            }

        }


        var texture = new THREE.DataTexture(
            data,
            this.size,
            this.size,
            THREE.RGBAFormat,
            THREE.FloatType
        );

        texture.minFilter =  THREE.NearestFilter,
            texture.magFilter = THREE.NearestFilter,

            texture.needsUpdate = true;

        this._reset(texture,pass);

    }

}

export default Composer;
