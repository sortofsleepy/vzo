/**
 * Styled logging function.
 * @type {{NOTICE: string, ERROR: string, WARNING: string, NOTICE_STYLE: string, ERROR_STYLE: string, log: Function}}
 */
var Console = {
    NOTICE:"NOTICE : ",
    ERROR:"ERROR : ",
    WARNING:"WARNING : ",

    NOTICE_STYLE:[
        "background:rgba(200,200,200,0.7);",
        "color:rgb(100,100,100);",
    ].join(""),

    ERROR_STYLE:[
        "background:rgba(0,0,0,1.0);",
        "color:yellow;",
    ].join(""),
    log:function(message,level="NOTICE"){
        switch(level){
            case "NOTICE":
                console.log("%c " + this.NOTICE + message,this.NOTICE_STYLE);
                break;

            case "WARNING":
                console.warn("%c " + this.WARNING + message,this.NOTICE_STYLE);
                break;

            case "ERROR":
                console.log("%c " + this.ERROR + message,this.ERROR_STYLE);
                break;

            default:
                console.log("%c " + "NOTICE : " + message,this.NOTICE_STYLE);
                break;
        }

    }
};


module.exports = Console;