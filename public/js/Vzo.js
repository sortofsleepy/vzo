(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({"./src/Main.js":[function(require,module,exports){
"use strict";

var _interopRequire = function (obj) { return obj && obj.__esModule ? obj["default"] : obj; };

var PGlobe = _interopRequire(require("./Globe.js"));

var SphereWorld = _interopRequire(require("./world/SphereWorld.js"));

var renderer = new THREE.WebGLRenderer({
    preserveDrawingBuffer: true
});


var scene = new THREE.Scene();
//var camera = new THREE.PerspectiveCamera(75.0,window.innerWidth / window.innerHeight, 1, 1000);
//camera.position.z = 100;

var camera = new THREE.PerspectiveCamera(27, window.innerWidth / window.innerHeight, 1, 3500);
camera.position.z = 1050;
camera.position.y = -215;

renderer.setSize(window.innerWidth, window.innerHeight);
renderer.setPixelRatio(window.devicePixelRatio);

document.body.appendChild(renderer.domElement);

/**============ SPHERE WORLD ===============*/
var sphere = new SphereWorld({
    radius: 900,
    widthSegments: 68,
    heightSegments: 66
});

var material = new THREE.MeshPhongMaterial({
    color: 11184810, specular: 16777215, shininess: 250,
    side: THREE.DoubleSide, vertexColors: THREE.VertexColors
});

//var mesh = new THREE.Mesh( sphere.getGeometry(), material );
var mesh = new THREE.Mesh(sphere.getGeometry(), material);
scene.add(sphere.getGeometry());

/**============ MAIN GLOBE ===============*/
var globe = new PGlobe(1000, renderer);
globe.addTo(scene);

//setup trackball control
var trackball = new THREE.TrackballControls(camera, renderer.domElement);


window.addEventListener("resize", function () {
    ASPECT_RATIO = window.innerWidth / window.innerHeight;
    camera.aspect = ASPECT_RATIO;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
});



animate();
function animate() {
    window.requestAnimationFrame(animate);
    globe.updateSim();
    trackball.update();


    renderer.render(scene, camera);
}

},{"./Globe.js":"/Users/sortofsleepy/Documents/apps/vzo/src/Globe.js","./world/SphereWorld.js":"/Users/sortofsleepy/Documents/apps/vzo/src/world/SphereWorld.js"}],"/Users/sortofsleepy/Documents/apps/vzo/node_modules/glslify/browser.js":[function(require,module,exports){
module.exports = noop

function noop() {
  throw new Error(
      'You should bundle your code ' +
      'using `glslify` as a transform.'
  )
}

},{}],"/Users/sortofsleepy/Documents/apps/vzo/node_modules/glslify/simple-adapter.js":[function(require,module,exports){
module.exports = programify

function programify(vertex, fragment, uniforms, attributes) {
  return {
    vertex: vertex, 
    fragment: fragment,
    uniforms: uniforms, 
    attributes: attributes
  };
}

},{}],"/Users/sortofsleepy/Documents/apps/vzo/src/Composer.js":[function(require,module,exports){
"use strict";

var _prototypeProperties = function (child, staticProps, instanceProps) { if (staticProps) Object.defineProperties(child, staticProps); if (instanceProps) Object.defineProperties(child.prototype, instanceProps); };

var _inherits = function (subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) subClass.__proto__ = superClass; };

var _classCallCheck = function (instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } };

/**
 * A class for performing GPU animations.
 *
 * Mostly based off of the work of Cabbibo and his
 * PhysicsRenderer for Three.js
 * https://github.com/cabbibo/PhysicsRenderer/
 */
var Composer = (function (_THREE$Geometry) {
    function Composer(size, renderer, options) {
        _classCallCheck(this, Composer);

        this._checkCapabilities(renderer);

        //options for the buffering
        var defaults = {
            width: size,
            height: size,
            wrapS: THREE.RepeatWrapping,
            wrapT: THREE.RepeatWrapping,
            minFilter: THREE.NearestFilter,
            magFilter: THREE.NearestFilter,
            format: THREE.RGBAFormat,
            stencilBuffer: false
        };
        this.options = options !== undefined ? options : defaults;

        if (options !== undefined) {
            this._extendOptions(defaults);
        }

        this.size = size;

        this.s2 = size * size;

        this.passes = [];

        this.passThru = this.passShader = this._createPassthru();

        this.renderer = renderer;
        this.camera = new THREE.OrthographicCamera(-0.5, 0.5, 0.5, -0.5, 0, 1);
        this.scene = new THREE.Scene();
        this.mesh = new THREE.Mesh(new THREE.PlaneBufferGeometry(1, 1));
        this.scene.add(this.mesh);
        this.counter = 0;
        this.clock = new THREE.Clock();
        this.resolution = new THREE.Vector2(this.size, this.size);

        window.addEventListener("resize", this.windowResize);
        this.windowRes = new THREE.Vector2(window.innerWidth, window.innerHeight);


        //original uniforms
        this.uniforms = {
            t_oPos: {
                type: "t",
                value: null
            },
            t_pos: {
                type: "t",
                value: null
            },
            resolution: {
                type: "v2",
                value: this.resolution
            },
            screenResolution: {
                type: "v2",
                value: this.windowRes
            }
        };

        this.numPasses = 0;
    }

    _inherits(Composer, _THREE$Geometry);

    _prototypeProperties(Composer, null, {
        windowResize: {

            /**
             * Update any params affected by window resizing here.
             * @param e
             */
            value: function windowResize(e) {
                this.windowRes.x = window.innerWidth;
                this.windowRes.y = window.innerHeight;

                //update the key uniforms like resolution on window resize
                for (var i = 0; i < this.passes.length; ++i) {
                    this.passes[i].uniforms.screenResolution.value = this.windowRes;
                }
            },
            writable: true,
            configurable: true
        },
        addPass: {

            /**
             * Adds a pass for calculatn things
             * @param name name for the pass
             * @param source fragment source for the pas
             * @param uniforms uniforms related to dealing with the pass.
             */
            value: function addPass(name, source, uniforms) {
                this.simulationUniforms = {
                    t_oPos: {
                        type: "t",
                        value: null
                    },
                    t_pos: {
                        type: "t",
                        value: null
                    },
                    resolution: {
                        type: "v2",
                        value: this.resolution
                    },
                    screenResolution: {
                        type: "v2",
                        value: this.windowRes
                    }
                };

                for (var i in this.uniforms) {
                    uniforms[i] = this.uniforms[i];
                }

                //adds a new simulation pass.
                var program = new THREE.ShaderMaterial({
                    uniforms: uniforms,
                    vertexShader: this.passThruVS,
                    fragmentShader: source
                });


                console.log(uniforms);
                var pass = {
                    name: name,
                    shader: program,
                    buffers: [this._generateRenderTargets(), this._generateRenderTargets(), this._generateRenderTargets()],
                    uniforms: uniforms
                };

                pass.rt_1 = pass.buffers[0];
                pass.rt_2 = pass.buffers[1];
                pass.rt_3 = pass.buffers[2];

                this._resetRand(30, pass);

                this.passes.push(pass);

                this.numPasses = this.passes.length;

                this.uniforms = uniforms;
            },
            writable: true,
            configurable: true
        },
        update: {
            value: function update() {
                for (var i = 0; i < this.numPasses; ++i) {
                    this.passes[i].uniforms.dT.value = performance.now() / 2 * 0.00009;
                    var flipFlop = this.counter % 3;

                    if (flipFlop == 0) {
                        this.passes[i].uniforms.t_oPos.value = this.passes[i].rt_1;
                        this.passes[i].uniforms.t_pos.value = this.passes[i].rt_2;

                        this._runPass(this.passes[i].shader, this.passes[i].rt_3);


                    } else if (flipFlop == 1) {
                        this.passes[i].uniforms.t_oPos.value = this.passes[i].rt_2;
                        this.passes[i].uniforms.t_pos.value = this.passes[i].rt_3;

                        this._runPass(this.passes[i].shader, this.passes[i].rt_1);



                    } else if (flipFlop == 2) {
                        this.passes[i].uniforms.t_oPos.value = this.passes[i].rt_3;
                        this.passes[i].uniforms.t_pos.value = this.passes[i].rt_1;

                        this._runPass(this.passes[i].shader, this.passes[i].rt_2);

                    }

                    this.counter++;
                }
            },
            writable: true,
            configurable: true
        },
        _extendOptions: {


            /**================ INTERNAL FUNCTIONS =======================*/
            /**
             * If the options aren't undefined, apply the defaults to fill in the gaps
             * @param defaults the default options.
             * @private
             */
            value: function _extendOptions(defaults) {
                for (var i in defaults) {
                    if (this.options[i] === undefined) {
                        this.options[i] = defaults[i];
                    }
                }
            },
            writable: true,
            configurable: true
        },
        _checkCapabilities: {

            /**
             * Makes sure that the hardware is capable of doing GPU calculations
             * @param renderer
             */
            value: function _checkCapabilities(renderer) {
                var gl = renderer.context;

                if (gl.getExtension("OES_texture_float") === null) {
                    alert("No Float Textures");
                    return;
                }

                if (gl.getParameter(gl.MAX_VERTEX_TEXTURE_IMAGE_UNITS) === 0) {
                    alert("Vert Shader Textures don't work");
                    return;
                }
            },
            writable: true,
            configurable: true
        },
        _createPassthru: {
            /**
             * Create passthru shader
             * @returns {THREE.ShaderMaterial}
             */
            value: function _createPassthru() {
                this.passThruVS = ["varying vec2 vUv;", "void main() {", "  vUv = uv;", "  gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );", "}"].join("\n");

                this.passThruFS = ["uniform sampler2D texture;", "varying vec2 vUv;", "void main() {", "  vec2 read = vUv;", "  vec4 c = texture2D( texture , vUv );", "  gl_FragColor = c ;",
                //   "gl_FragColor = vec4(1.);",
                "}"].join("\n");

                var uniforms = {
                    texture: { type: "t", value: null }
                };

                var texturePassShader = new THREE.ShaderMaterial({
                    uniforms: uniforms,
                    vertexShader: this.passThruVS,
                    fragmentShader: this.passThruFS
                });

                return texturePassShader;
            },
            writable: true,
            configurable: true
        },
        _runPass: {

            /**
             * Runs through a pass
             * @param shader
             * @param target
             */
            value: function _runPass(shader, target) {
                this.mesh.material = shader;
                this.renderer.render(this.scene, this.camera, target, false);
            },
            writable: true,
            configurable: true
        },
        _generateRenderTargets: {

            /**
             * Generates a render target
             * @param data any data that might want to get applied onto the target
             * @returns {THREE.WebGLRenderTarget}
             * @private
             */
            value: function _generateRenderTargets(data) {
                var options = this.options;

                var target = new THREE.WebGLRenderTarget(this.size, this.size, {
                    minFilter: THREE.NearestFilter,
                    magFilter: THREE.NearestFilter,
                    format: THREE.RGBAFormat,
                    type: THREE.FloatType,
                    stencilBuffer: false
                });


                return target;
            },
            writable: true,
            configurable: true
        },
        _generateDataTexture: {


            /**
             * Generates a data texture.
             * @param data optional precalculated data
             * @returns {THREE.DataTexture} returns a DataTexture
             * @private
             */
            value: function _generateDataTexture(data) {
                var a = null;

                if (data !== undefined) {
                    a = data;
                } else {
                    a = new Float32Array(this.options.width * this.options.height * 3);
                    for (var k = 0, kl = a.length; k < kl; k += 3) {
                        var x = Math.random() - 0.5;
                        var y = Math.random() - 0.5;
                        var z = Math.random() - 0.5;

                        a[k + 0] = x * 10;
                        a[k + 1] = y * 10;
                        a[k + 2] = z * 10;
                    }
                }


                var texture = new THREE.DataTexture(a, this.options.width, this.options.height, THREE.RGBFormat, THREE.FloatType);
                texture.minFilter = THREE.NearestFilter;
                texture.magFilter = THREE.NearestFilter;
                texture.needsUpdate = true;
                texture.flipY = false;

                return texture;
            },
            writable: true,
            configurable: true
        },
        _reset: {
            value: function _reset(texture, pass) {
                this.texture = texture;
                this.passThru.uniforms.texture.value = texture;

                this._runPass(this.passThru, pass.buffers[0]);
                this._runPass(this.passThru, pass.buffers[1]);
                this._runPass(this.passThru, pass.buffers[2]);
            },
            writable: true,
            configurable: true
        },
        _resetRand: {
            value: function _resetRand(size, pass, alpha) {
                var size = size || 100;
                var data = new Float32Array(this.s2 * 4);

                for (var i = 0; i < data.length; i++) {
                    //console.log('ss');
                    data[i] = (Math.random() - 0.5) * size;

                    if (alpha && i % 4 === 3) {
                        data[i] = 0;
                    }
                }


                var texture = new THREE.DataTexture(data, this.size, this.size, THREE.RGBAFormat, THREE.FloatType);

                texture.minFilter = THREE.NearestFilter, texture.magFilter = THREE.NearestFilter, texture.needsUpdate = true;

                this._reset(texture, pass);
            },
            writable: true,
            configurable: true
        }
    });

    return Composer;
})(THREE.Geometry);

module.exports = Composer;

},{}],"/Users/sortofsleepy/Documents/apps/vzo/src/Globe.js":[function(require,module,exports){
"use strict";

var _interopRequire = function(obj) {
    return (obj && obj.__esModule ? obj["default"] : obj);
};

var _prototypeProperties = function(child, staticProps, instanceProps) {
    if (staticProps)
        Object.defineProperties(child, staticProps);

    if (instanceProps)
        Object.defineProperties(child.prototype, instanceProps);
};

var _get = function get(object, property, receiver) {
    var desc = Object.getOwnPropertyDescriptor(object, property);

    if (desc === undefined) {
        var parent = Object.getPrototypeOf(object);

        if (parent === null) {
            return undefined;
        } else {
            return get(parent, property, receiver);
        }
    } else if ("value" in desc && desc.writable) {
        return desc.value;
    } else {
        var getter = desc.get;

        if (getter === undefined) {
            return undefined;
        }

        return getter.call(receiver);
    }
};

var _inherits = function(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
        throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
    }

    subClass.prototype = Object.create(superClass && superClass.prototype, {
        constructor: {
            value: subClass,
            enumerable: false,
            writable: true,
            configurable: true
        }
    });

    if (superClass)
        subClass.__proto__ = superClass;
};

var _classCallCheck = function(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
        throw new TypeError("Cannot call a class as a function");
    }
};

var Composer = _interopRequire(require("./Composer"));
var AudioTexture = _interopRequire(require("./audio/AudioTexture"));
var glslify = require("glslify");

var Globe = function(Composer) {
    function Globe(size, renderer, options) {
        _classCallCheck(this, Globe);
        _get(Object.getPrototypeOf(Globe.prototype), "constructor", this).call(this, size, renderer, options);
        var audio = new AudioTexture("./audio/mother.mp3");
        audio.update();
        var parent = this;

        window.addEventListener("keydown", function() {
            audio.play();
            var playVal = parent.uniforms.isPlaying.value;

            if (playVal == 1) {
                parent.uniforms.isPlaying.value = 0;
            } else {
                parent.uniforms.isPlaying.value = 1;
            }
        });

        var i = 0;
        var texture = new TG.Texture(256, 256).add(new TG.XOR().tint(1, 0.5, 0.7)).add(new TG.SinX().frequency(0.004).tint(0.5, 0, 0)).mul(new TG.SinY().frequency(0.004).tint(0.5, 0, 0)).add(new TG.SinX().frequency(0.0065).tint(0.1, 0.5, 0.2)).add(new TG.SinY().frequency(0.0065).tint(0.5, 0.5, 0.5)).add(new TG.Noise().tint(0.1, 0.1, 0.2)).toCanvas();
        this.threeTexture = new THREE.Texture(texture);
        this.threeTexture.needsUpdate = true;
        var renderShader = require("glslify/simple-adapter.js")("\n#define GLSLIFY 1\n\nuniform sampler2D t_pos;\nvarying vec2 vUv;\nvoid main() {\n  vUv = uv;\n  vec4 pos = texture2D(t_pos, position.xy);\n  vec3 dif = cameraPosition - pos.xyz;\n  vec4 mvPosition = modelViewMatrix * vec4(position, 1.0);\n  gl_PointSize = 20.0 * (300.0 / length(mvPosition.xyz));\n  gl_Position = projectionMatrix * modelViewMatrix * vec4(pos.xyz, 1.);\n}", "\n#define GLSLIFY 1\n\nuniform float test;\nuniform sampler2D overlay;\nvarying vec2 vUv;\nvoid main() {\n  float p = test;\n  vec4 oColor = texture2D(overlay, vUv);\n  gl_FragColor = oColor;\n}", [{"name":"t_pos","type":"sampler2D"},{"name":"test","type":"float"},{"name":"overlay","type":"sampler2D"}], []);
        var simulationSource = require("glslify/simple-adapter.js")("\n#define GLSLIFY 1\n\nuniform sampler2D t_oPos;\nuniform sampler2D t_pos;\nuniform sampler2D audioTexture;\nuniform vec2 resolution;\nuniform int isPlaying;\nuniform float dT;\nuniform vec3 centerPos;\nvarying vec2 vUv;\nvoid main() {\n  vec2 test = vUv;\n  vec2 uv = gl_FragCoord.xy / resolution;\n  vec4 oPos = texture2D(t_oPos, uv);\n  vec4 pos = texture2D(t_pos, uv);\n  vec4 audio = texture2D(audioTexture, vec2(uv.y, 1.0));\n  vec3 vel = pos.xyz - oPos.xyz;\n  vec3 force = vec3(0.);\n  vec3 dif = pos.xyz - centerPos;\n  float al = length(audio);\n  force -= length(al) * length(al) * normalize(dif);\n  if(isPlaying == 1) {\n    vel += force * audio.x;\n  } else if(isPlaying == 0) {\n    \n  }\n  vec3 p = pos.xyz + vel;\n  gl_FragColor = vec4(p, 1.);\n}", "\n#define GLSLIFY 1\n\nuniform sampler2D t_oPos;\nuniform sampler2D t_pos;\nuniform sampler2D audioTexture;\nuniform vec2 resolution;\nuniform int isPlaying;\nuniform float dT;\nuniform vec3 centerPos;\nvarying vec2 vUv;\nvoid main() {\n  vec2 test = vUv;\n  vec2 uv = gl_FragCoord.xy / resolution;\n  vec4 oPos = texture2D(t_oPos, uv);\n  vec4 pos = texture2D(t_pos, uv);\n  vec4 audio = texture2D(audioTexture, vec2(uv.y, 1.0));\n  vec3 vel = pos.xyz - oPos.xyz;\n  vec3 force = vec3(0.);\n  vec3 dif = pos.xyz - centerPos;\n  float al = length(audio);\n  force -= length(al) * length(al) * normalize(dif);\n  if(isPlaying == 1) {\n    vel += force * audio.x;\n  } else if(isPlaying == 0) {\n    \n  }\n  vec3 p = pos.xyz + vel;\n  gl_FragColor = vec4(p, 1.);\n}", [{"name":"t_oPos","type":"sampler2D"},{"name":"t_pos","type":"sampler2D"},{"name":"audioTexture","type":"sampler2D"},{"name":"resolution","type":"vec2"},{"name":"isPlaying","type":"int"},{"name":"dT","type":"float"},{"name":"centerPos","type":"vec3"},{"name":"t_oPos","type":"sampler2D"},{"name":"t_pos","type":"sampler2D"},{"name":"audioTexture","type":"sampler2D"},{"name":"resolution","type":"vec2"},{"name":"isPlaying","type":"int"},{"name":"dT","type":"float"},{"name":"centerPos","type":"vec3"}], []);

        this.uniforms.audioTexture = {
            type: "t",
            value: audio.getTexture()
        };

        this.uniforms.beatTexture = {
            type: "t",
            value: audio.getBeatsTexture()
        };

        this.addPass("gravity", simulationSource.fragment, {
            dT: {
                type: "f",
                value: 0
            },

            isPlaying: {
                type: "i",
                value: 0
            },

            overlay: {
                type: "t",
                value: null
            },

            centerPoss: {
                type: "v3",
                value: new THREE.Vector3()
            }
        });

        this.audio = audio;

        this.mat = new THREE.ShaderMaterial({
            uniforms: {
                test: {
                    type: "f",
                    value: 1
                }
            },

            vertexShader: renderShader.vertex,
            fragmentShader: renderShader.fragment
        });

        this.generateGeometry(size);
        this.particles = new THREE.PointCloud(this.geo, this.mat);
        this.particles.frustumCulled = false;
    }

    _inherits(Globe, Composer);

    _prototypeProperties(Globe, null, {
        updateSim: {
            value: function updateSim() {
                this.update();
            },

            writable: true,
            configurable: true
        },

        generateGeometry: {
            value: function generateGeometry(particleCount) {
                var size = particleCount;
                var particleCount = 1000;
                var r = 1;
                this.oscillatorSpeed = 0.5;
                this.targetVertices = [];
                var positions = new Float32Array(particleCount * 3);
                var colors = new Float32Array(particleCount * 4);
                this.psize = positions;
                this.radius = 0;
                this.positions = positions;

                for (var i = 0; i < positions.length; ++i) {
                    var vec = new THREE.Vector3();
                    vec.x = Math.random();
                    vec.y = Math.random();
                    vec.z = Math.random();
                    vec.normalize();
                    vec.multiplyScalar(r);
                    this.targetVertices.push(vec.x);
                    this.targetVertices.push(vec.y);
                    this.targetVertices.push(vec.z);
                }

                for (var i = 0; i < positions.length; ++i) {
                    positions[i] = this.targetVertices[i];
                    positions[i + 1] = this.targetVertices[i + 1];
                    positions[i + 2] = this.targetVertices[i + 2];
                }

                this.oscillator = 0;
                var geometry = new THREE.BufferGeometry();
                var color = new THREE.Color();

                for (var i = 0; i < colors.length; ++i) {
                    color.setHSL(i / particleCount, 1, 0.5);
                    colors[i] = color.r;
                    colors[i + 1] = color.g;
                    colors[i + 2] = color.b;
                    colors[i + 3] = Math.random();
                }

                geometry.addAttribute("position", new THREE.BufferAttribute(positions, 3));
                geometry.addAttribute("colors", new THREE.BufferAttribute(colors, 4));
                geometry.addAttribute("targetVertex", new THREE.BufferAttribute(new Float32Array(this.targetVertices), 3));
                this.parameters = [[0.25, new THREE.Color(11730431), 1, 2], [0.5, new THREE.Color(10676438), 1, 1], [0.75, new THREE.Color(12386226), 0.75, 1], [1, new THREE.Color(10676398), 0.5, 1], [1.25, new THREE.Color(2099), 0.8, 1]];
                this.length = this.parameters.length;
                this.lines = [];

                for (i = 0; i < this.parameters.length; ++i) {
                    var p = this.parameters[i];
                    var line = new THREE.Line(geometry, this.mat);
                    line.scale.x = line.scale.y = line.scale.z = p[0];
                    line.originalScale = p[0];
                    line.rotation.y = Math.random() * Math.PI;
                    line.updateMatrix();
                    this.lines.push(line);
                }

                this.geo = geometry;
            },

            writable: true,
            configurable: true
        },

        addTo: {
            value: function addTo(scene) {
                for (var i = 0; i < this.lines.length; ++i) {
                    scene.add(this.lines[i]);
                }
            },

            writable: true,
            configurable: true
        }
    });

    return Globe;
}(Composer);

module.exports = Globe;
},{"./Composer":"/Users/sortofsleepy/Documents/apps/vzo/src/Composer.js","./audio/AudioTexture":"/Users/sortofsleepy/Documents/apps/vzo/src/audio/AudioTexture.js","glslify":"/Users/sortofsleepy/Documents/apps/vzo/node_modules/glslify/browser.js","glslify/simple-adapter.js":"/Users/sortofsleepy/Documents/apps/vzo/node_modules/glslify/simple-adapter.js"}],"/Users/sortofsleepy/Documents/apps/vzo/src/audio/AudioBoxer.js":[function(require,module,exports){
"use strict";

var _prototypeProperties = function (child, staticProps, instanceProps) { if (staticProps) Object.defineProperties(child, staticProps); if (instanceProps) Object.defineProperties(child.prototype, instanceProps); };

var _classCallCheck = function (instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } };

/**
 * A class for dealing with WebAudio and WebGL.
 *
 * Based off of the work of some amazing artists.
 *
 * http://cabbi.bo/dotter/lib/AudioController.js
 * http://www.airtightinteractive.com/demos/js/uberviz/audioanalysis
 * @sortofsleepy
 */
var AudioBoxer = (function () {
    function AudioBoxer(file, options) {
        _classCallCheck(this, AudioBoxer);

        var defaults = {
            frequencyBinCount: 1024,
            looping: false,
            gain: 0.5,
            fadeTime: 0.4,
            fftSize: 1024,
            smoothingTimeConstant: 0.8,
            volumeSensitivity: 0.5,
            levelsCount: 16
        };

        this.options = options !== undefined ? options : defaults;

        if (options !== undefined) {}

        var ctx = window.AudioContext || window.webkitAudioContext;
        this.ctx = ctx = new ctx();

        this.isPlaying = false;

        this.pitchCurrent = 0;
        this.pitchLerped = 0;
        this.volumeCurrent = 0;
        this.volumeLerped = 0;
        this.lerpFactor = 0.2;

        this.waveData = [];
        this.levelsData = [];
        this.level = 0;
        this.bpmTime = 0;
        this.ratedBPMTime = 550;
        this.levelHistory = [];

        //BPM related
        this.bpmStart = 1;
        this.BEAT_HOLD_TIME = 40;
        this.BEAT_DECAY_RATE = 0.98;
        this.BEAT_MIN = 0.15;
        this.beatTime = 0;
        this.count = 0;
        this.msecsFirst = 0;
        this.msecsPrevious = 0;
        this.msecsAvg = 633; // time between beats.
        this.levelBins = Math.floor(this.options.frequencyBinCount / this.options.levelsCount); //number of bins in each level
        this.beatCutOff = 0;
        this.gotBeat = false;

        //setup some nodes
        this.analyser = ctx.createAnalyser();
        this.analyser.array = new Uint8Array(this.options.frequencyBinCount);
        this.freqByteData = new Uint8Array(this.analyser.frequencyBinCount);
        this.timeByteData = new Uint8Array(this.options.frequencyBinCount);

        this.filter = ctx.createBiquadFilter();
        this.gain = this.ctx.createGain();

        this.analyser.frequencyBinCount = 128;
        this.analyser.array = new Uint8Array(128);

        this.file = file !== undefined ? file : false;
        var element = new Audio();

        //load audio if it's not undefined
        if (this.file !== false) {
            /**
             * Alternate method of streaming audio using <audio> instead of
             * having to directly load the sound first
             */
            this.src = ctx.createMediaElementSource(element);


            element.src = this.file;

            //need to make sure this appends to the body, otherwise the play() function doesn't work.
            element.style.position = "absolute";
            element.style.opacity = 0;
            document.body.appendChild(element);

            this.domElement = element;


            //connect things if we have audio
            this.connectComponents();
        }
    }

    _prototypeProperties(AudioBoxer, null, {
        connectComponents: {

            /**
             * Connects all the nodes together
             */
            value: function connectComponents(source) {
                this.gain.connect(this.analyser);
                this.src.connect(this.gain);
                this.src.connect(this.ctx.destination);
            },
            writable: true,
            configurable: true
        },
        fadeOut: {

            /**
             * Fades out the track.
             * @param time
             * @param callback
             */
            value: function fadeOut(time, callback) {
                var t = this.ctx.currentTime;
                if (!time) {
                    time = this.options.fadeTime;
                }
                this.gain.linearRampToValueAtTime(this.gain.value, t);
                this.gain.linearRampToValueAtTime(0, t + time);
                setTimeout(callback.bind(this), time * 1000);
            },
            writable: true,
            configurable: true
        },
        fadeIn: {

            /**
             * Fades in track
             * @param time
             * @param value
             */
            value: function fadeIn(time, value) {
                this.gain.linearRampToValueAtTime(1, this.ctx.currentTime + time);
            },
            writable: true,
            configurable: true
        },
        updateData: {

            /**
             * Updates the analyser with new data
             */
            value: function updateData() {
                if (this.isPlaying) {
                    this.analyser.getByteFrequencyData(this.analyser.array);
                    this.analyser.getByteFrequencyData(this.freqByteData);
                    this.analyser.getByteTimeDomainData(this.timeByteData);

                    //normalize waveform
                    this.normalizeWaveform();

                    //normalize levels
                    this.normalizeLevels();
                }
            },
            writable: true,
            configurable: true
        },
        normalizeWaveform: {
            value: function normalizeWaveform() {
                for (var i = 0; i < this.analyser.frequencyBinCount; ++i) {
                    this.waveData[i] = (this.timeByteData[i] - 128) / 128 * this.options.volumeSensitivity;
                }
            },
            writable: true,
            configurable: true
        },
        normalizeLevels: {
            value: function normalizeLevels() {
                for (var i = 0; i < this.options.levelsCount; ++i) {
                    var sum = 0;
                    for (var j = 0; j < this.levelBins; j++) {
                        sum += this.freqByteData[i * this.levelBins + j];
                    }
                    this.levelsData[i] = sum / this.levelBins / 256 * this.options.volumeSensitivity; //freqData maxs at 256

                    //adjust for the fact that lower levels are percieved more quietly
                    //make lower levels smaller
                    //levelsData[i] *=  1 + (i/levelsCount)/2;
                }

                //set the average level
                var sum = 0;
                for (var j = 0; j < this.options.levelsCount; ++j) {
                    sum += this.levelsData[j];
                }

                this.level = sum / this.options.levelsCount;
                this.levelHistory.push(this.level);
                // this.levelHistory.shift();

                this.computeBeat();
            },
            writable: true,
            configurable: true
        },
        computeBeat: {
            value: function computeBeat() {
                if (this.level > this.beatCutOff && this.level > this.BEAT_MIN) {
                    this.beatCutOff = this.level * 1.1;
                    this.beatTime = 0;
                } else {
                    if (this.beatTime <= this.BEAT_HOLD_TIME) {
                        this.beatTime++;
                    } else {
                        this.beatCutOff *= this.BEAT_DECAY_RATE;
                        this.beatCutOff = Math.max(this.beatCutOff, this.BEAT_MIN);
                    }
                }
                // console.log(this.bpmStart,this.msecsAvg);
                this.bpmTime = (new Date().getTime() - this.bpmStart) / this.msecsAvg;
            },
            writable: true,
            configurable: true
        },
        play: {

            /**======================= AUDIO PLAYBACK ====================*/

            value: function play() {
                if (this.isPlaying) {
                    this.isPlaying = false;
                    this.domElement.pause();
                } else {
                    this.domElement.play();
                    this.isPlaying = true;
                }
            },
            writable: true,
            configurable: true
        },
        playFromBeginning: {

            /**
             * Resets the playhead to the beginning
             */
            value: function playFromBeginning() {
                this.domElement.currentTime = 0;
            },
            writable: true,
            configurable: true
        }
    });

    return AudioBoxer;
})();

module.exports = AudioBoxer;
//TODO currently needs polyfill
//Object.assign(this.options,defaults);

},{}],"/Users/sortofsleepy/Documents/apps/vzo/src/audio/AudioTexture.js":[function(require,module,exports){
"use strict";

var _interopRequire = function (obj) { return obj && obj.__esModule ? obj["default"] : obj; };

var _prototypeProperties = function (child, staticProps, instanceProps) { if (staticProps) Object.defineProperties(child, staticProps); if (instanceProps) Object.defineProperties(child.prototype, instanceProps); };

var _get = function get(object, property, receiver) { var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc && desc.writable) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _inherits = function (subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) subClass.__proto__ = superClass; };

var _classCallCheck = function (instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } };

/**
 * A class for dealing with WebAudio,WebGL and texture data.
 * Based off of the work of others.
 *
 * http://www.airtightinteractive.com/demos/js/uberviz/audioanalysis/js/AudioHandler.js
 * http://cabbi.bo/
 *
 * @sortofsleepy
 */
var AudioBoxer = _interopRequire(require("./AudioBoxer.js"));

var AudioTexture = (function (AudioBoxer) {
    /**
     * Constructor
     * @param file path to audio file
     * @param options any additional options we want for the audio processor
     */
    function AudioTexture(file, options) {
        _classCallCheck(this, AudioTexture);

        _get(Object.getPrototypeOf(AudioTexture.prototype), "constructor", this).call(this, file, options);

        this.width = this.options.frequencyBinCount / 4;
        this.notes = [];
        var data = this.processAudio();
        var beats = this.processBeats();
        /**
         * If we're using THREE, init a data texture
         *
         * TODO include ability to create generalized WebGL texture;
         */
        if (THREE !== undefined) {
            this.texture = new THREE.DataTexture(data, data.length / 16, 1, THREE.RGBAFormat, THREE.FloatType);
            this.beatsTexture = new THREE.DataTexture(beats, beats.length / 16, 1, THREE.RGBAFormat, THREE.FloatType);
            this.texture.needsUpdate = true;
        }
    }

    _inherits(AudioTexture, AudioBoxer);

    _prototypeProperties(AudioTexture, null, {
        update: {

            /**
             * Updates our audio data.
             * No need to put this in render loop
             */
            value: function update() {
                var parent = this;

                update();
                function update() {
                    window.requestAnimationFrame(update);
                    parent.updateData();

                    //update texture with audio data
                    parent.texture.image.data = parent.processAudio();
                    parent.texture.needsUpdate = true;
                }
            },
            writable: true,
            configurable: true
        },
        getTexture: {

            /**
             * Returns a texture containing general audio data
             * @returns {AudioTexture.texture|*}
             */
            value: function getTexture() {
                return this.texture;
            },
            writable: true,
            configurable: true
        },
        getBeatsTexture: {

            /**
             * Returns the texture holding the level history
             * @returns {AudioTexture.beatsTexture|*}
             */
            value: function getBeatsTexture() {
                return this.beatsTexture;
            },
            writable: true,
            configurable: true
        },
        getBpmTime: {

            /**
             * Returns a normalized bpm time
             * @returns {THREE.Vector2.x|*}
             */
            value: function getBpmTime() {
                var vec = new THREE.Vector2(this.bpmTime, 0);
                vec.normalize();
                return vec.x;
            },
            writable: true,
            configurable: true
        },
        processBeats: {

            //TODO figure out if this is actually a way to process beats
            value: function processBeats() {
                var data = new Float32Array(this.width * 4);
                var beats = this.bpmTime;
                var levels = this.levelHistory;

                for (var i = 0; i < this.width; i += 4) {
                    data[i] = levels[i] / 256;
                    data[i + 1] = levels[i + 1] / 256;
                    data[i + 2] = levels[i + 2] / 256;
                    data[i + 3] = levels[i + 3] / 256;
                }

                return data;
            },
            writable: true,
            configurable: true
        },
        processAudio: {
            value: function processAudio() {
                //TODO maybe move this out and re-fill instead of creating new one each time
                var data = new Float32Array(this.width * 4);
                var beats = this.bpmTime;
                var levels = this.levelHistory;

                for (var i = 0; i < this.width; i += 4) {
                    data[i] = this.analyser.array[i / 4] / 256;
                    data[i + 1] = this.analyser.array[i / 4 + 1] / 256;
                    data[i + 2] = this.analyser.array[i / 4 + 2] / 256;
                    data[i + 3] = this.analyser.array[i / 4 + 3] / 256;
                }



                return data;
            },
            writable: true,
            configurable: true
        }
    });

    return AudioTexture;
})(AudioBoxer);

module.exports = AudioTexture;

},{"./AudioBoxer.js":"/Users/sortofsleepy/Documents/apps/vzo/src/audio/AudioBoxer.js"}],"/Users/sortofsleepy/Documents/apps/vzo/src/world/SphereWorld.js":[function(require,module,exports){
"use strict";

var _prototypeProperties = function (child, staticProps, instanceProps) { if (staticProps) Object.defineProperties(child, staticProps); if (instanceProps) Object.defineProperties(child.prototype, instanceProps); };

var _inherits = function (subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) subClass.__proto__ = superClass; };

var _classCallCheck = function (instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } };

var SphereWorld = (function (_THREE$Geometry) {
    function SphereWorld(options, gui) {
        _classCallCheck(this, SphereWorld);

        THREE.Geometry.call(this);

        this.type = "SphereGeometry";

        this.defaults = {
            radius: 50,
            widthSegments: Math.random() * 99,
            heightSegments: 6,
            phiStart: 0,
            phiLength: Math.PI * 2,
            thetaStart: 0,
            thetaLength: Math.PI
        };

        options = options != undefined ? options : this.defaults;

        options = this._extend(options);



        this._buildGeometry(gui);
    }

    _inherits(SphereWorld, _THREE$Geometry);

    _prototypeProperties(SphereWorld, null, {
        _buildGeometry: {
            value: function _buildGeometry(gui) {
                var options = this.options;
                var radius = options.radius;


                var widthSegments = options.widthSegments;
                var heightSegments = options.heightSegments;

                var phiStart = options.phiStart;
                var phiLength = options.phiLength;

                var thetaStart = options.thetaStart;
                var thetaLength = options.thetaLength;


                var x,
                    y,
                    vertices = [],
                    uvs = [];

                for (y = 0; y <= heightSegments; y++) {
                    var verticesRow = [];
                    var uvsRow = [];

                    for (x = 0; x <= widthSegments; x++) {
                        var u = x / widthSegments;
                        var v = y / heightSegments;

                        var vertex = new THREE.Vector3();
                        vertex.x = -radius * Math.cos(phiStart + u * phiLength) * Math.sin(thetaStart + v * thetaLength);
                        vertex.y = radius * Math.cos(thetaStart + v * thetaLength);
                        vertex.z = radius * Math.sin(phiStart + u * phiLength) * Math.sin(thetaStart + v * thetaLength);

                        this.vertices.push(vertex);

                        verticesRow.push(this.vertices.length - 1);
                        uvsRow.push(new THREE.Vector2(u, 1 - v));
                    }

                    vertices.push(verticesRow);
                    uvs.push(uvsRow);
                }

                for (y = 0; y < heightSegments; y++) {
                    for (x = 0; x < widthSegments; x++) {
                        var v1 = vertices[y][x + 1];
                        var v2 = vertices[y][x];
                        var v3 = vertices[y + 1][x];
                        var v4 = vertices[y + 1][x + 1];

                        var n1 = this.vertices[v1].clone().normalize();
                        var n2 = this.vertices[v2].clone().normalize();
                        var n3 = this.vertices[v3].clone().normalize();
                        var n4 = this.vertices[v4].clone().normalize();

                        var uv1 = uvs[y][x + 1].clone();
                        var uv2 = uvs[y][x].clone();
                        var uv3 = uvs[y + 1][x].clone();
                        var uv4 = uvs[y + 1][x + 1].clone();

                        if (Math.abs(this.vertices[v1].y) === radius) {
                            uv1.x = (uv1.x + uv2.x) / 2;
                            this.faces.push(new THREE.Face3(v1, v3, v4, [n1, n3, n4]));
                            this.faceVertexUvs[0].push([uv1, uv3, uv4]);
                        } else if (Math.abs(this.vertices[v3].y) === radius) {
                            uv3.x = (uv3.x + uv4.x) / 2;
                            this.faces.push(new THREE.Face3(v1, v2, v3, [n1, n2, n3]));
                            this.faceVertexUvs[0].push([uv1, uv2, uv3]);
                        } else {
                            this.faces.push(new THREE.Face3(v1, v2, v4, [n1, n2, n4]));
                            this.faceVertexUvs[0].push([uv1, uv2, uv4]);

                            this.faces.push(new THREE.Face3(v2, v3, v4, [n2.clone(), n3, n4.clone()]));
                            this.faceVertexUvs[0].push([uv2.clone(), uv3, uv4.clone()]);
                        }
                    }
                }

                this.computeFaceNormals();

                this.boundingSphere = new THREE.Sphere(new THREE.Vector3(), radius);

                ////////// SETUP BUFFER GEOMETRY /////////

                var triangles = 160000;
                var n = 800,
                    n2 = n / 2; // triangles spread in the cube
                var d = 12,
                    d2 = d / 2; // individual triangle size

                var points = this.vertices;
                var flat = [];
                for (var i = 0; i < points.length; ++i) {
                    flat.push(points[i].x);
                    flat.push(points[i].y);
                    flat.push(points[i].z);
                }


                var particles = flat.length;

                var geometry = new THREE.BufferGeometry();

                var positions = new Float32Array(particles);
                var colors = new Float32Array(particles);

                var color = new THREE.Color();

                var n = 1000,
                    n2 = n / 2; // particles spread in the cube

                for (var i = 0; i < positions.length; i += 3) {
                    // positions

                    var x = Math.random() * n - n2;
                    var y = Math.random() * n - n2;
                    var z = Math.random() * n - n2;

                    positions[i] = flat[i];
                    positions[i + 1] = flat[i + 1];
                    positions[i + 2] = flat[i + 2];

                    // colors

                    var vx = x / n + 0.5;
                    var vy = y / n + 0.5;
                    var vz = z / n + 0.5;

                    color.setRGB(vx, vy, vz);

                    colors[i] = color.r;
                    colors[i + 1] = color.g;
                    colors[i + 2] = color.b;
                }

                geometry.addAttribute("position", new THREE.BufferAttribute(positions, 3));
                geometry.addAttribute("color", new THREE.BufferAttribute(colors, 3));

                //console.log(positions);
                geometry.computeBoundingSphere();
                var material = new THREE.PointCloudMaterial({ size: 155, vertexColors: THREE.VertexColors });
                // var material = new THREE.ShaderMaterial({
                //
                //     vertexShader:document.getElementById("vertex-s").textContent,
                //     fragmentShader:document.getElementById("fragment-s").textContent
                // });
                var particleSystem = new THREE.Mesh(geometry, material);


                particleSystem.rotation.x = 140;


                this.geo = particleSystem;
            },
            writable: true,
            configurable: true
        },
        rotateY: {
            value: function rotateY(deg) {
                this.geo.rotation.y = deg;
            },
            writable: true,
            configurable: true
        },
        rotateX: {
            value: function rotateX(deg) {
                this.geo.rotation.x = deg;
            },
            writable: true,
            configurable: true
        },
        rotateZ: {
            value: function rotateZ(deg) {
                this.geo.rotation.z = deg;
            },
            writable: true,
            configurable: true
        },
        getGeometry: {
            value: function getGeometry() {
                return this.geo;
            },
            writable: true,
            configurable: true
        },
        _extend: {
            value: function _extend(options) {
                for (var i in this.defaults) {
                    if (!options.hasOwnProperty(i)) {
                        options[i] = this.defaults[i];
                    }
                }

                this.options = options;

                return this.options;
            },
            writable: true,
            configurable: true
        }
    });

    return SphereWorld;
})(THREE.Geometry);

module.exports = SphereWorld;

},{}]},{},["./src/Main.js"]);

//# sourceMappingURL=Vzo.js.map