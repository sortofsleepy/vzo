var gulp = require("gulp");
var gutil = require("gulp-util");
var sourcemaps = require("gulp-sourcemaps");
var source = require("vinyl-source-stream");
var buffer = require("vinyl-buffer");
var watchify = require("watchify");
var browserify = require("browserify");
var babelify = require("babelify")
var browserSync = require("browser-sync");
var glslify = require("glslify");


var util = require("util");
var exec = require("child_process").exec


var fs = require("fs");

//setup entry point for js
var bundler = watchify(browserify('./src/Main.js',watchify.args));
bundler.transform(babelify);




gulp.task('js',bundle);
bundler.on('update',bundle);
/**
 * Bundles JS together
 * @returns {*}
 */
function bundle(){
    browserSync.reload();
    return bundler.bundle()
        .on("error",gutil.log.bind(gutil,"Browserify Error"))
        .pipe(source('Vzo.js'))
        .pipe(buffer())
        .pipe(sourcemaps.init({loadMaps:false}))
        .pipe(sourcemaps.write("./"))
        .pipe(gulp.dest("./public/js"));
}

///////////// DEFAULT ////////////////

/**
 *  Runs glslify on all the shaders in the shaders folder of the root directory.
 */
gulp.task("glsl",function(){
    var directory = "./shaders/";
    var compileDir = "./src/shaders/";

    var dir = fs.readdirSync(directory);

    var command = "";
    for(var i = 0;i<dir.length;++i){
        var file = dir[i];

        var baseCommand = "glslify " + directory + file + " > " + compileDir + file ;
        if(i == 0){
            command += baseCommand + " | ";
        }else{
            command += baseCommand;
        }

    }


    var child = exec(command,function(err,stdout,stderr){
        if(err){
            console.log(err);
        }
        if(stderr){
            console.log(stderr);
        }

        console.log("Glslifying done.\n");
    });

    //copy compiled shaders to public folder.
    return gulp.src([compileDir + "**/*.glsl"],{

    }).pipe(gulp.dest("./public/shaders"));
});



gulp.task("default",["js"],function(){
    browserSync({
        server:{
            baseDir: "./public"
        }
    });

    gulp.watch(["./src/**/*.js"],["js"]);
   // gulp.watch(["./shaders/**/*.glsl"],["js"]);
});